package com.aa.tonigdx;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Toni on 25.01.2016.
 */

public class ToniGDXGameConfig {
    private Color clearColor = Color.WHITE;
    private String skinFilePath = "gfx/gui/myskin.json";
    private String textureAtlasFilePath = "gfx/texture_atlas.atlas";
    private String animationsFilePath = "gfx/animations.json";

    public Color getClearColor() {
        return clearColor;
    }

    public void setClearColor(Color clearColor) {
        this.clearColor = clearColor;
    }

    public String getSkinFilePath() {
        return skinFilePath;
    }

    public void setSkinFilePath(String skinFilePath) {
        this.skinFilePath = skinFilePath;
    }

    public String getTextureAtlasFilePath() {
        return textureAtlasFilePath;
    }

    public void setTextureAtlasFilePath(String textureAtlasFilePath) {
        this.textureAtlasFilePath = textureAtlasFilePath;
    }

    public String getAnimationsFilePath() {
        return animationsFilePath;
    }

    public void setAnimationsFilePath(String animationsFilePath) {
        this.animationsFilePath = animationsFilePath;
    }
}
