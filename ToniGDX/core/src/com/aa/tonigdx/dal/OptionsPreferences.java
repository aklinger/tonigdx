package com.aa.tonigdx.dal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Saves the configurations of the options to Preferences.
 * <p>
 * @author Toni
 */
public class OptionsPreferences {
    //Attributes
    private final Preferences prefs;

    //Constructors
    public OptionsPreferences(String preferencesName) {
        prefs = Gdx.app.getPreferences(preferencesName);
    }

    //Getter & Setter
    public boolean getBoolean(String key, boolean defValue) {
        return prefs.getBoolean(key, defValue);
    }

    public String getString(String key, String defValue) {
        return prefs.getString(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return prefs.getFloat(key, defValue);
    }

    public int getInteger(String key, int defValue) {
        return prefs.getInteger(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return prefs.getLong(key, defValue);
    }

    public void setBoolean(String key, boolean value) {
        prefs.putBoolean(key, value);
        prefs.flush();
    }

    public void setString(String key, String value) {
        prefs.putString(key, value);
        prefs.flush();
    }

    public void setFloat(String key, float value) {
        prefs.putFloat(key, value);
        prefs.flush();
    }

    public void setInteger(String key, int value) {
        prefs.putInteger(key, value);
        prefs.flush();
    }

    public void setLong(String key, long value) {
        prefs.putLong(key, value);
        prefs.flush();
    }
}
