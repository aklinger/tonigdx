package com.aa.tonigdx.dal;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * An abstract way of accessing Resources.
 * <p/>
 * @author Toni
 */
public interface ResourceProvider {
    //Methods
    public TextureRegion getTexture(String name);
}
