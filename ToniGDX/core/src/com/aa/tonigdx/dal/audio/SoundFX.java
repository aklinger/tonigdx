package com.aa.tonigdx.dal.audio;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by Toni on 28.04.2014.
 */
public class SoundFX {
    private Sound sound;
    private long soundId;

    public SoundFX(Sound sound, long soundId) {
        this.sound = sound;
        this.soundId = soundId;
    }

    public void stopThisInstance() {
        sound.stop(soundId);
    }

    public void setLooping(boolean looping) {
        sound.setLooping(soundId, looping);
    }

    public void setVolume(float volume) {
        sound.setVolume(soundId, volume);
    }

    public Sound getSound() {
        return sound;
    }

    public long getSoundId() {
        return soundId;
    }
}
