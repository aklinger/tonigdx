package com.aa.tonigdx.dal.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages a Cache of sound Effects.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class SoundManager {
    private float volume = 1f;
    private boolean enabled = true;
    private boolean playSilentSounds = false;
    private final Map<String, Sound> sounds;

    private boolean adjustAudio;
    private Vector2 listenerPosition = new Vector2();
    private float fullVolumeRange = 800;
    private float noVolumeRange = 1000;

    private static SoundManager instance;

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    public SoundManager() {
        sounds = new HashMap<>();
    }

    private Vector2 temp = new Vector2();

    public float calculateAdjustmentForListener(Vector2 soundPosition) {
        temp.set(soundPosition);
        float dist = temp.sub(listenerPosition).len();
        if (dist <= fullVolumeRange) {
            return 1;
        } else if (dist > noVolumeRange) {
            return 0;
        } else {
            return 1 - ((dist - fullVolumeRange) / (noVolumeRange - fullVolumeRange));
        }
    }

    public SoundFX playRandom(String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        return play(sounds[chosen], 1, listenerPosition, playSilentSounds);
    }

    public SoundFX playRandom(float volumeMultiplier, String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        return play(sounds[chosen], volumeMultiplier, listenerPosition, playSilentSounds);
    }

    public SoundFX playRandom(float volumeMultiplier, Vector2 position, String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        return play(sounds[chosen], volumeMultiplier, position, playSilentSounds);
    }

    public SoundFX playRandom(float volumeMultiplier, Vector2 position, boolean playAlsoSilent, String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        return play(sounds[chosen], volumeMultiplier, position, playAlsoSilent);
    }

    public SoundFX play(String sound) {
        return play(sound, 1, listenerPosition, playSilentSounds);
    }

    public SoundFX play(String sound, float volumeMultiplier) {
        return play(sound, volumeMultiplier, listenerPosition, playSilentSounds);
    }

    public SoundFX play(String sound, float volumeMultiplier, boolean playAlsoSilent) {
        return play(sound, volumeMultiplier, listenerPosition, playAlsoSilent);
    }

    public SoundFX play(String sound, float volumeMultiplier, Vector2 position) {
        return play(sound, volumeMultiplier, position, playSilentSounds);
    }

    public SoundFX play(String sound, float volumeMultiplier, Vector2 position, boolean playAlsoSilent) {
        if (!enabled) {
            if (playAlsoSilent) {
                volumeMultiplier = 0f;
            } else {
                return null;
            }
        }

        float adjustment = 1;
        if (adjustAudio) {
            adjustment = calculateAdjustmentForListener(position);
        }
        float finalVolume = this.volume * volumeMultiplier * adjustment;
        if (finalVolume <= 0 && !playAlsoSilent) {
            return null;
        }
        finalVolume = MathUtils.clamp(finalVolume, 0f, 1f);

        Sound soundToPlay = getSound(sound);

        long id = soundToPlay.play(finalVolume);//TODO: allow setting of pitch and pan
        return new SoundFX(soundToPlay, id); //TODO: create a Pool
    }

    public Sound getSound(String sound) {
        Sound soundToPlay = sounds.get(sound);
        if (soundToPlay == null) {
            return load(sound);
        }
        return soundToPlay;
    }

    public Sound load(String sound) {
        FileHandle soundFile = Gdx.files.internal(sound);
        Sound soundToLoad = Gdx.audio.newSound(soundFile);
        sounds.put(sound, soundToLoad);
        return soundToLoad;
    }

    public void dispose() {
        for (Sound sound : sounds.values()) {
            sound.stop();
            sound.dispose();
        }
        sounds.clear();
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public float getVolume() {
        return volume;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setListenerPosition(Vector2 listenerPosition) {
        this.listenerPosition = listenerPosition;
    }

    public Vector2 getListenerPosition() {
        return listenerPosition;
    }

    public void setFullVolumeRange(float fullVolumeRange) {
        this.fullVolumeRange = fullVolumeRange;
    }

    public void setNoVolumeRange(float noVolumeRange) {
        this.noVolumeRange = noVolumeRange;
    }

    public void setAdjustAudio(boolean adjustAudio) {
        this.adjustAudio = adjustAudio;
    }

    public boolean isPlaySilentSounds() {
        return playSilentSounds;
    }

    public void setPlaySilentSounds(boolean playSilentSounds) {
        this.playSilentSounds = playSilentSounds;
    }
}
