package com.aa.tonigdx.dal.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MusicWrapper implements Music {

    private MusicManager musicManager;

    private String musicName;
    private Music delegate;
    private float trackVolume;
    private boolean loop;

    public static boolean allowLocalExternal = false;

    public MusicWrapper(MusicManager musicManager, String musicName, float trackVolume) {
        this.musicManager = musicManager;
        this.musicName = musicName;
        this.trackVolume = trackVolume;
        this.loop = true;
    }

    public void load(String musicName) {
        this.musicName = musicName;
        reload();
    }

    public void reload() {
        dispose();
        boolean successfullyLoaded = false;
        if (allowLocalExternal) {
            try {
                this.delegate = Gdx.audio.newMusic(Gdx.files.local(musicName));
                if (this.delegate != null) {
                    successfullyLoaded = true;
                    Gdx.app.log("MUSIC", "External music loading enabled. Found: " + musicName);
                }
            } catch (GdxRuntimeException ignored) {
                Gdx.app.log("MUSIC", "External music loading enabled. Searched: " + musicName);
            }
        }
        if (!successfullyLoaded) {
            this.delegate = Gdx.audio.newMusic(Gdx.files.internal(musicName));
        }
        this.delegate.setVolume(trackVolume * musicManager.getMasterVolume());
        this.delegate.setLooping(loop);
    }

    public void loadIfNecessary() {
        if (delegate == null) {
            reload();
        }
    }

    public void setDelegate(Music delegate) {
        this.delegate = delegate;
    }

    public Music getDelegate() {
        return delegate;
    }

    @Override
    public void play() {
        if (delegate != null) {
            delegate.play();
        }
    }

    @Override
    public void pause() {
        if (delegate != null) {
            delegate.pause();
        }
    }

    @Override
    public void stop() {
        if (delegate != null) {
            delegate.stop();
        }
    }

    @Override
    public boolean isPlaying() {
        return delegate != null && delegate.isPlaying();
    }

    @Override
    public void setLooping(boolean loop) {
        if (delegate != null) {
            delegate.setLooping(loop);
        }
        this.loop = loop;
    }

    @Override
    public boolean isLooping() {
        return loop;
    }

    public void updateVolume() {
        if (delegate != null) {
            delegate.setVolume(trackVolume * musicManager.getMasterVolume());
        }
    }

    @Override
    public void setVolume(float volume) {
        this.trackVolume = volume;
        updateVolume();
    }

    @Override
    public float getVolume() {
        return trackVolume;
    }

    @Override
    public void setPan(float pan, float volume) {
        delegate.setPan(pan, volume);
    }

    @Override
    public void setPosition(float position) {
        delegate.setPosition(position);
    }

    @Override
    public float getPosition() {
        return delegate.getPosition();
    }

    @Override
    public void dispose() {
        if (delegate != null) {
            delegate.dispose();
            delegate = null;
        }
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener onCompletionListener) {
        delegate.setOnCompletionListener(onCompletionListener);
    }

    public String getMusicName() {
        return musicName;
    }
}
