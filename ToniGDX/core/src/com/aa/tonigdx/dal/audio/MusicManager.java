package com.aa.tonigdx.dal.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

/**
 * A service that manages the background music.
 * <p/>
 * Allows for mixing multiple Music tracks.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class MusicManager implements Disposable {
    /**
     * Holds the music currently being played, if any.
     */
    private Array<MusicWrapper> musicBeingPlayed;

    /**
     * The masterVolume to be set on the music.
     */
    private float masterVolume = 1f;

    /**
     * Whether the music is enabled.
     */
    private boolean enabled = true;

    /**
     * Creates the music manager.
     */
    private MusicManager() {
        musicBeingPlayed = new Array<>(2);
    }

    private static MusicManager instance;

    public static MusicManager getInstance() {
        if (instance == null) {
            instance = new MusicManager();
        }
        return instance;
    }

    /**
     * Plays the given music (starts the streaming).
     * <p/>
     * If there is already a music being played it is stopAndDisposed
     * automatically.
     */
    public void play(String musicName) {
        playMultiple(musicName);
    }

    /**
     * Loads the given music tracks (starts the streaming).
     * <p>
     * Per default sets the volume of all tracks that are not the first to zero.
     *
     * @param musicNames The file paths of the music tracks. For Gdx.files.internal().
     */
    public void playMultiple(String... musicNames) {
        stopAndDispose();

        for (int i = 0; i < musicNames.length; i++) {
            float volume = i == 0 ? 1 : 0;
            MusicWrapper musicWrapper = new MusicWrapper(this, musicNames[i], volume);
            musicBeingPlayed.add(musicWrapper);
            musicWrapper.setLooping(true);
        }
        if (enabled) {
            loadAndStart();
        }
    }

    public void playIfNotAlreadyPlaying(String musicName) {
        playIfNotAlreadyPlayingMultiple(musicName);
    }

    public void playIfNotAlreadyPlayingMultiple(String... musicNames) {
        boolean same = true;
        if (musicNames.length == musicBeingPlayed.size) {
            for (int i = 0; i < musicNames.length; i++) {
                if (!musicNames[i].equals(musicBeingPlayed.get(i).getMusicName())) {
                    same = false;
                }
            }
        } else {
            same = false;
        }
        if (same && enabled) {
            for (MusicWrapper musicWrapper : musicBeingPlayed) {
                if (!musicWrapper.isPlaying()) {
                    musicWrapper.stop();//rewind playback position
                    musicWrapper.play();
                }
            }
        } else {
            playMultiple(musicNames);
        }
    }

    /**
     * Stops and disposes the current music being played, if any.
     */
    public void stopAndDispose() {
        for (MusicWrapper musicWrapper : musicBeingPlayed) {
            musicWrapper.stop();
            musicWrapper.dispose();
        }
        musicBeingPlayed.clear();
    }

    public void pause() {
        for (MusicWrapper musicWrapper : musicBeingPlayed) {
            musicWrapper.pause();
        }
    }

    public void resume() {
        for (MusicWrapper musicWrapper : musicBeingPlayed) {
            musicWrapper.play();
        }
    }

    /**
     * Sets the music masterVolume which should be inside the range [0,1].
     */
    public void setMasterVolume(float masterVolume) {
        if (masterVolume < 0 || masterVolume > 1f) {
            Gdx.app.log("MUSIC", "The masterVolume should be inside the range: [0,1]");
        }
        masterVolume = MathUtils.clamp(masterVolume, 0, 1);
        if (this.masterVolume != masterVolume) {
            this.masterVolume = masterVolume;

            // if there is a music being played, change its volume
            for (MusicWrapper musicWrapper : musicBeingPlayed) {
                musicWrapper.updateVolume();
            }
        }
    }

    /**
     * Sets the music for individual tracks to transition between them.
     *
     * @param trackVolumes An array of the volumes of the tracks in order.
     */
    public void setTrackVolume(float... trackVolumes) {
        for (int i = 0; i < musicBeingPlayed.size; i++) {
            MusicWrapper musicWrapper = musicBeingPlayed.get(i);
            musicWrapper.setVolume(trackVolumes[MathUtils.clamp(i, 0, trackVolumes.length - 1)]);
        }
    }

    /**
     * Enables or disabled the music.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;

        if (enabled) {
            loadAndStart();
        } else {
            //should I really dispose music objects when the game is muted? pausing should be enough
            pause();
        }
    }

    public void loadAndStart() {
        for (MusicWrapper musicWrapper : musicBeingPlayed) {
            musicWrapper.loadIfNecessary();
        }
        resume();
    }

    /**
     * Disposes the music manager.
     */
    @Override
    public void dispose() {
        stopAndDispose();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public float getMasterVolume() {
        return masterVolume;
    }

    public int getTrackCount() {
        return musicBeingPlayed.size;
    }

    public MusicWrapper getMusicTrack(int index) {
        return musicBeingPlayed.get(index);
    }

    public Array<MusicWrapper> getMusicBeingPlayed() {
        return musicBeingPlayed;
    }
}
