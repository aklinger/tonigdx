package com.aa.tonigdx.dal;

import com.aa.tonigdx.dal.graphics.SpriteSheet;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Loads and stores all the Textures.
 *
 * @author Toni
 */
public class TextureManager implements ResourceProvider {

    private ObjectMap<String, String> nameMap;
    private ObjectMap<String, SpriteSheet> spriteSheets;
    private TextureAtlas textureAtlas;
    private static TextureManager instance;

    private TextureManager() {
        nameMap = new ObjectMap<>();
        spriteSheets = new ObjectMap<>();
        textureAtlas = new TextureAtlas();
    }

    public static TextureManager getInstance() {
        if (instance == null) {
            instance = new TextureManager();
        }
        return instance;
    }

    public void loadTexturesFromAtlas(String file) {
        if (textureAtlas != null) {
            textureAtlas.dispose();
        }
        if (!file.endsWith(".atlas")) {
            file = file + ".atlas";
        }
        textureAtlas = new TextureAtlas(Gdx.files.internal(file));
    }

    public void loadTextureMetaData(String file) throws IOException {
        FileHandle fileHandle = Gdx.files.internal(file);
        BufferedReader reader = fileHandle.reader(256);
        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.isEmpty() && !line.startsWith("#")) {
                String[] split = line.split(":");
                if (split.length >= 2) {
                    nameMap.put(split[1], split[0]);
                }
            }
        }
    }

    @Override
    public TextureRegion getTexture(String name) {
        if (textureAtlas != null) {
            String alt = nameMap.get(name);
            TextureRegion textureRegion;
            if (alt != null) {
                textureRegion = textureAtlas.findRegion(alt);
            } else {
                textureRegion = textureAtlas.findRegion(name);
            }
            if (textureRegion == null) {
                throw new RuntimeException("Couldn't load texture: " + name);
            }
            return textureRegion;
        } else {
            throw new RuntimeException("No texture atlas loaded, couldn't load texture: " + name);
        }
    }

    public SpriteSheet getSpriteSheet(String spriteSheetName) {
        SpriteSheet spriteSheet = spriteSheets.get(spriteSheetName);
        if (spriteSheet == null) {
            throw new RuntimeException("Couldn't find spriteSheet: " + spriteSheetName);
        }
        return spriteSheet;
    }

    public TextureRegion getTextureFromSpriteSheet(String spriteSheetName, int spriteIndex) {
        SpriteSheet spriteSheet = getSpriteSheet(spriteSheetName);
        TextureRegion region = spriteSheet.getRegion(spriteIndex);
        if (region == null) {
            throw new RuntimeException("Index (" + spriteIndex + ") out of range for spriteSheet: " + spriteSheetName);
        }
        return region;
    }

    public void loadSpriteSheet(String name, int x_cols, int y_rows, boolean ignoreDuplicate) {
        TextureRegion texture = TextureManager.getInstance().getTexture(name);
        if (texture == null) {
            throw new RuntimeException("Texture not found: " + name);
        }
        addSpriteSheet(SpriteSheet.simpleGridSheet(texture, x_cols, y_rows), name, ignoreDuplicate);
    }

    public SpriteSheet tryToLoadSpriteSheet(String name, FileHandle fileHandle, boolean ignoreDuplicate) {
        if (fileHandle.exists()) {
            TextureRegion texture = TextureManager.getInstance().getTexture(name);
            if (texture == null) {
                throw new RuntimeException("Texture not found: " + name);
            }
            SpriteSheet spriteSheet = SpriteSheet.spriteSheetFromAsepriteJsonFile(texture, fileHandle);
            addSpriteSheet(spriteSheet, name, ignoreDuplicate);
            return spriteSheet;
        }
        return null;
    }

    private void addSpriteSheet(SpriteSheet spriteSheet, String name, boolean ignoreDuplicate) {
        SpriteSheet alreadyPresent = spriteSheets.get(name);
        if (alreadyPresent != null) {
            if (ignoreDuplicate) {
                return;
            }
            throw new RuntimeException("Duplicate declaration of sprite-sheet: " + name + ".");
        }
        spriteSheets.put(name, spriteSheet);
    }

    public void disposeAllTextures() {
        textureAtlas.dispose();
        textureAtlas.getRegions().clear();
        spriteSheets.clear();
    }

    public void loadTextureRegion(String name, TextureRegion region) {
        textureAtlas.addRegion(name, region);
    }

    public TextureAtlas getTextureAtlas() {
        return textureAtlas;
    }
}
