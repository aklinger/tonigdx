package com.aa.tonigdx.dal.graphics;

import com.aa.tonigdx.logic.animation.AnimationSheet;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.io.BufferedReader;
import java.util.Arrays;

/**
 * Created by Toni on 14.01.2016.
 */
public class SpriteSheet {
    private TextureRegion[] regions;

    public SpriteSheet() {
    }

    public static SpriteSheet spriteSheetFromAsepriteJsonFile(TextureRegion sheet, FileHandle spritesheetData) {
        BufferedReader reader = spritesheetData.reader(256);
        JsonReader jsonReader = new JsonReader();
        JsonValue rootElement = jsonReader.parse(reader);
        if (rootElement == null) {
            throw new RuntimeException("Could not parse JSON file: " + spritesheetData.name());
        }

        SpriteSheet spriteSheet = new SpriteSheet();

        JsonValue meta = rootElement.get("meta");
        int width = meta.get("size").getInt("w");
        int height = meta.get("size").getInt("h");
        if (width != sheet.getRegionWidth() || height != sheet.getRegionHeight()) {
            throw new RuntimeException("SpriteSheet dimensions don't match meta data. Metadata out of date?");
        }

        JsonValue frames = rootElement.get("frames");
        spriteSheet.regions = new TextureRegion[frames.size];

        int counter = 0;
        for (JsonValue value : frames.iterator()) {
            JsonValue frame = value.get("frame");
            int x = frame.getInt("x");
            int y = frame.getInt("y");
            int w = frame.getInt("w");
            int h = frame.getInt("h");
            TextureRegion textureRegion = new TextureRegion(sheet, x, y, w, h);
            spriteSheet.regions[counter] = textureRegion;
            counter++;
        }
        return spriteSheet;
    }


    public static SpriteSheet simpleGridSheet(TextureRegion sheet, int cols, int rows) {
        int tileWidth = sheet.getRegionWidth() / cols;
        int tileHeight = sheet.getRegionHeight() / rows;

        SpriteSheet spriteSheet = new SpriteSheet();
        spriteSheet.regions = new TextureRegion[rows * cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                spriteSheet.regions[row * cols + col] = new TextureRegion(sheet, col * tileWidth, row * tileHeight, tileWidth, tileHeight);
            }
        }

        return spriteSheet;
    }

    public TextureRegion getRegion(int index) {
        if (index >= 0 && index < regions.length) {
            return regions[index];
        } else {
            return null;
        }
    }

    public AnimationSheet createAnimationSheet(int frameDuration) {
        return createAnimationSheet(frameDuration, 0, regions.length);
    }

    public AnimationSheet createAnimationSheet(int frameDuration, int startIndex, int endIndex) {
        AnimationSheet animationSheet = new AnimationSheet();
        Animation<TextureRegion> animation = createAnimation(frameDuration, startIndex, endIndex);
        animationSheet.putAnimation(AnimationSheet.ANIMATION_DEFAULT, animation);
        return animationSheet;
    }

    public Animation<TextureRegion> createAnimation(int frameDuration, int startIndex, int endIndex) {
        return new Animation<>(frameDuration, Arrays.copyOfRange(regions, startIndex, endIndex));
    }
}
