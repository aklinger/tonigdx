package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public interface GifEncoder {
    void createAnimatedGif(FileHandle file, Array<Pixmap> frames, GifResult callback);
}
