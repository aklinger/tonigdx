package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public class ScreenRecorderStub implements ScreenRecorder {
    @Override
    public boolean startRecording(int maxFrames) {
        return false;
    }

    @Override
    public void finishRecording() {

    }

    @Override
    public boolean recordFrame() {
        return false;
    }

    @Override
    public boolean isRecording() {
        return false;
    }

    @Override
    public Array<Pixmap> getRecordedFrames() {
        return null;
    }
}
