package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public interface ScreenRecorder {
    boolean startRecording(int maxFrames);

    void finishRecording();

    boolean recordFrame();

    boolean isRecording();

    Array<Pixmap> getRecordedFrames();
}
