package com.aa.tonigdx.dal.gif;

public abstract class GifResult implements Runnable {
    private boolean success;
    private String filename;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
