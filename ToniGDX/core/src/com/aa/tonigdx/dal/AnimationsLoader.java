package com.aa.tonigdx.dal;

import com.aa.tonigdx.dal.graphics.SpriteSheet;
import com.aa.tonigdx.logic.animation.AnimationSheet;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;

import java.io.BufferedReader;

/**
 * Created by Toni on 15.04.2015.
 */
public class AnimationsLoader {
    public static AnimationsLoader getInstance() {
        if (instance == null) {
            instance = new AnimationsLoader();
        }
        return instance;
    }

    private static AnimationsLoader instance;

    private String spriteFolderPath;
    private ObjectMap<String, AnimationSheet> animationSheets;

    public AnimationsLoader() {
        this.animationSheets = new ObjectMap<>();
    }

    public ObjectMap<String, AnimationSheet> loadAnimations(FileHandle fileHandle) {
        spriteFolderPath = fileHandle.parent().path() + "/sprites/";
        BufferedReader reader = fileHandle.reader(256);
        JsonReader jsonReader = new JsonReader();
        JsonValue value = jsonReader.parse(reader);
        if (value == null) {
            throw new RuntimeException("Could not parse JSON file: " + fileHandle.name());
        }
        return parseAnimations(value);
    }

    protected ObjectMap<String, AnimationSheet> parseAnimations(JsonValue jsonValue) {
        ObjectMap<String, AnimationSheet> animationSheetMap = new ObjectMap<>();

        JsonValue array = jsonValue.get("sprites");
        for (JsonValue value : array.iterator()) {
            String animationSheetName = value.getString("name");
            if (animationSheetMap.get(animationSheetName) != null) {
                throw new RuntimeException("Duplicate AnimationSheet name: " + animationSheetName);
            }
            try {
                AnimationSheet animationSheet = parseAnimationSheet(value);
                animationSheetMap.put(animationSheetName, animationSheet);
            } catch (RuntimeException runtimeException) {
                throw new RuntimeException("Failed to parse AnimationSheet: " + animationSheetName, runtimeException);
            }
        }

        return animationSheetMap;
    }

    protected AnimationSheet parseAnimationSheet(JsonValue jsonValue) {
        AnimationSheet animationSheet = new AnimationSheet("default");
        JsonValue sheets = jsonValue.get("sheets");
        if (sheets != null) {
            for (JsonValue value : sheets.iterator()) {
                parseSpriteSheet(value);
            }
        }
        JsonValue array = jsonValue.get("animations");
        for (JsonValue value : array.iterator()) {
            Animation<TextureRegion> animation = parseAnimation(value);
            String animationName = value.getString("name");
            if (animation.getKeyFrames().length <= 0) {
                throw new RuntimeException("Failed to load animation: " + animationName);
            }
            if (animationSheet.getAnimation(animationName) != null) {
                throw new RuntimeException("Duplicate declaration of animation: " + animationName);
            }
            animationSheet.putAnimation(animationName, animation);
        }
        return animationSheet;
    }

    protected void parseSpriteSheet(JsonValue spriteSheet) {
        String name = spriteSheet.getString("sheet-name");
        if (name == null || name.isEmpty()) {
            throw new RuntimeException("Missing name for sprite-sheet.");
        }
        String dim = spriteSheet.getString("dimensions");
        if (dim == null) {
            throw new RuntimeException("Missing dimensions from sprite-sheet: " + name);
        }
        int cols = 1;
        int rows = 1;
        if (!dim.contains("x")) {
            Gdx.app.log("AnimationsLoader", "Missing separator 'x' for dimensions. Assuming 1 row, and given value as columns.");
            cols = Integer.parseInt(dim);
            rows = 1;
        } else {
            String[] dimensions = dim.split("x");
            cols = Integer.parseInt(dimensions[0]);
            rows = Integer.parseInt(dimensions[1]);
        }
        TextureManager.getInstance().loadSpriteSheet(name, cols, rows, false);
    }

    protected Animation<TextureRegion> parseAnimation(JsonValue jsonValue) {
        float frameDuration = 1;
        if (jsonValue.has("frameDuration")) {
            frameDuration = jsonValue.getFloat("frameDuration");
        }
        Array<TextureRegion> frames = new Array<>(TextureRegion.class);
        JsonValue framesArray = jsonValue.get("frames");
        for (JsonValue value : framesArray.iterator()) {
            String frameName = value.toString();
            TextureRegion texture;
            if (frameName.contains("#")) {
                String[] spriteSheetName = frameName.split("#");
                SpriteSheet spriteSheetCache;
                try {
                    spriteSheetCache = TextureManager.getInstance().getSpriteSheet(spriteSheetName[0]);
                } catch (RuntimeException exception) {
                    spriteSheetCache = TextureManager.getInstance().tryToLoadSpriteSheet(spriteSheetName[0], Gdx.files.internal(spriteFolderPath + spriteSheetName[0] + ".json"), false);
                }
                if (spriteSheetCache == null) {
                    Gdx.app.log("AnimationsLoader", "spriteFolderPath = " + spriteFolderPath + spriteSheetName[0]);
                    throw new RuntimeException("Failed to load texture of sprite sheet: " + frameName);
                }
                String[] tokens = spriteSheetName[1].split(",");
                for (String token : tokens) {
                    String[] loops = token.split("-");
                    int loopBegin = Integer.parseInt(loops[0]);
                    int loopEnd = loopBegin;
                    if (loops.length > 1) {
                        loopEnd = Integer.parseInt(loops[1]);
                    }
                    int step = ((loopEnd - loopBegin) < 0 ? -1 : 1);
                    for (int j = loopBegin; j != loopEnd + step; j += step) {
                        texture = spriteSheetCache.getRegion(j);
                        if (texture == null) {
                            throw new RuntimeException("Index " + j + " out of range for spriteSheet: " + spriteSheetName[0]);
                        }
                        frames.add(texture);
                    }
                }
            } else {
                texture = TextureManager.getInstance().getTexture(frameName);
                if (texture == null) {
                    throw new RuntimeException("Failed to load texture: " + frameName);
                }
                frames.add(texture);
            }
        }
        Animation.PlayMode mode = Animation.PlayMode.NORMAL;
        if (jsonValue.has("mode")) {
            mode = Animation.PlayMode.valueOf(jsonValue.getString("mode").toUpperCase());
        }
        return new Animation<>(frameDuration, frames, mode);
    }

    public AnimationSheet getAnimationSheetInstance(String identifier) {
        return this.getAnimationSheetInstance(identifier, false);
    }

    public AnimationSheet getAnimationSheetInstance(String identifier, boolean createStaticFromTexture) {
        AnimationSheet animationSheet = animationSheets.get(identifier);
        if (animationSheet == null) {
            if (createStaticFromTexture) {
                try {
                    TextureRegion texture = TextureManager.getInstance().getTexture(identifier);
                    animationSheet = new AnimationSheet();
                    animationSheet.putAnimation(AnimationSheet.ANIMATION_DEFAULT, new Animation<>(1, texture));
                    animationSheet.getAnimation(AnimationSheet.ANIMATION_DEFAULT).setPlayMode(Animation.PlayMode.LOOP);
                    animationSheets.put(identifier, animationSheet);
                } catch (RuntimeException ex) {
                    throw new RuntimeException("Couldn't load AnimationSheet from static texture: " + identifier);
                }
            } else {
                throw new RuntimeException("Couldn't load AnimationSheet: " + identifier);
            }
        }
        return animationSheet.shallowCopy();
    }

    public void cacheAnimationSheets(FileHandle fileHandle) {
        ObjectMap<String, AnimationSheet> entries = loadAnimations(fileHandle);
        for (ObjectMap.Entry<String, AnimationSheet> entry : entries) {
            if (animationSheets.get(entry.key) != null) {
                Gdx.app.log("AnimationSheet", "[WARNING] AnimationSheet " + entry.key + " already loaded. Will be overridden.");
            }
            animationSheets.put(entry.key, entry.value);
        }
    }

    public void disposeAll() {
        animationSheets.clear();
    }
}
