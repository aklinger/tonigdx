package com.aa.tonigdx.dal.input.remap;

import com.aa.tonigdx.dal.input.InputAction;
import com.aa.tonigdx.dal.input.PlayerInputController;
import com.aa.tonigdx.dal.input.mappings.*;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by toni on 7/21/16.
 */
public class Remapper extends InputAdapter implements ControllerListener {

    private static Remapper instance;

    public static Remapper getInstance() {
        if (instance == null) {
            instance = new Remapper();
        }
        return instance;
    }

    private boolean additive;
    private boolean remapping = false;
    private PlayerInputController playerInputController;
    private InputAction actionToMap;
    private static final int CANCEL_KEY_ESC = Input.Keys.ESCAPE;

    private static final float axisRemapTreshold = 0.9f;
    private static final float triggerRemapThreshold = 0.5f;
    private ObjectMap<Integer, Float> lowestAbsAxisValuesSinceLastRemap = new ObjectMap<>();

    private boolean escapeCancelsRemapping = true;
    private boolean swallowSuccessfullRemap = false;
    private boolean allowDiagonalPov = false;

    private Array<RemapEventListener> remapEventListenerArray = new Array<>();

    public void clearListeners() {
        remapEventListenerArray.clear();
    }

    public void addListener(RemapEventListener remapEventListener) {
        remapEventListenerArray.add(remapEventListener);
    }

    private boolean updateListeners(RemapEvent remapEvent) {
        for (RemapEventListener listener : remapEventListenerArray) {
            boolean result = listener.somethingRemapped(remapEvent);
            if (result) {
                return true;
            }
        }
        return false;
    }

    public void cancelRemap() {
        remapping = false;
        updateListeners(new RemapEvent(RemapEvent.Type.REMAP_CANCELLED, null));
    }

    public void finishRemapping() {
        remapping = false;
        //TODO: fix nested iterator exception with listener collection
        //updateListeners(new RemapEvent(RemapEvent.Type.REMAP_END, null));
    }

    public void listenForRemapOfAction(InputAction action, PlayerInputController playerInputController, boolean additive) {
        this.additive = additive;
        actionToMap = action;
        this.playerInputController = playerInputController;
        remapping = true;
        initAxisValues();
    }

    protected void initAxisValues() {
        Controller assignedController = playerInputController.getAssignedController();
        if (assignedController != null) {
            for (int i = 0; i < 6; i++) {
                lowestAbsAxisValuesSinceLastRemap.put(i, Math.abs(assignedController.getAxis(i)));
            }
        }
    }

    public boolean isRemappingAction(InputAction action) {
        return remapping && action.equals(actionToMap);
    }

    public boolean isRemapping() {
        return remapping;
    }

    public InputAction getActionToMap() {
        return actionToMap;
    }

    private boolean storeRemap(InputState inputState) {
        InputState intention;
        if (additive) {
            intention = new MultiState();
            ((MultiState) intention).getInputStateList().add(inputState);

            InputState assignedInputState = playerInputController.getAssignedInputState(actionToMap);
            if (assignedInputState != null) {
                if (assignedInputState instanceof MultiState) {
                    for (InputState oldStates : ((MultiState) assignedInputState).getInputStateList()) {
                        ((MultiState) intention).getInputStateList().add(oldStates);
                    }
                } else {
                    ((MultiState) intention).getInputStateList().add(assignedInputState);
                }
            }
        } else {
            intention = inputState;
        }
        boolean cancelRemap = updateListeners(new RemapEvent(RemapEvent.Type.REMAP_INTENTION, intention));
        if (!cancelRemap) {
            playerInputController.setActionMapping(actionToMap, intention);
            remapping = false;
            updateListeners(new RemapEvent(RemapEvent.Type.KEY_REMAPPED, intention));
        }
        return swallowSuccessfullRemap;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (remapping) {
            if (escapeCancelsRemapping && keycode == CANCEL_KEY_ESC) {
                cancelRemap();
                return true;
            }
            return storeRemap(new KeyMapping(keycode));
        }
        return false;
    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        if (remapping && this.playerInputController.getAssignedController() == controller) {
            return storeRemap(new ButtonMapping(buttonCode));
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        if (remapping && this.playerInputController.getAssignedController() == controller) {
            float valueAbs = Math.abs(value);
            Float lowestAbsValue = lowestAbsAxisValuesSinceLastRemap.get(axisCode);
            if (lowestAbsValue == null || valueAbs < lowestAbsValue) {
                lowestAbsValue = valueAbs;
                lowestAbsAxisValuesSinceLastRemap.put(axisCode, lowestAbsValue);
            }
            if (lowestAbsValue < triggerRemapThreshold && valueAbs > axisRemapTreshold || valueAbs >= 1f) {
                boolean invert = (value < 0);
                return storeRemap(new MultiAxisMapping(invert, axisCode));
            }
        }
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        if (remapping && this.playerInputController.getAssignedController() == controller) {
            if (!PovDirection.center.equals(value) && (allowDiagonalPov || !isPovDiagonal(value))) {
                return storeRemap(new PovMapping(povCode, value));
            }
        }
        return false;
    }

    public boolean isPovDiagonal(PovDirection povDirection) {
        return povDirection == PovDirection.northEast || povDirection == PovDirection.northWest
                || povDirection == PovDirection.southEast || povDirection == PovDirection.southWest;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        //TODO: remap to sliders?
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    public void setSwallowSuccessfullRemap(boolean swallowSuccessfullRemap) {
        this.swallowSuccessfullRemap = swallowSuccessfullRemap;
    }

    public void setEscapeCancelsRemapping(boolean escapeCancelsRemapping) {
        this.escapeCancelsRemapping = escapeCancelsRemapping;
    }

    public void setAllowDiagonalPov(boolean allowDiagonalPov) {
        this.allowDiagonalPov = allowDiagonalPov;
    }
}
