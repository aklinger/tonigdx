package com.aa.tonigdx.dal.input.remap;

import com.aa.tonigdx.dal.input.mappings.InputState;

/**
 * Created by toni on 10/9/16.
 */
public class RemapEvent {
    public enum Type {
        REMAP_INTENTION, KEY_REMAPPED, REMAP_CANCELLED, REMAP_END;
    }

    private Type type;
    private InputState state;

    public RemapEvent(Type type, InputState state) {
        this.type = type;
        this.state = state;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public InputState getState() {
        return state;
    }

    public void setState(InputState state) {
        this.state = state;
    }
}
