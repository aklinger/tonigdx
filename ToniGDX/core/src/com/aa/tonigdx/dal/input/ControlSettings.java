package com.aa.tonigdx.dal.input;

import com.aa.tonigdx.dal.input.mappings.InputState;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by Toni on 12.07.2014.
 */
public class ControlSettings implements InputProcessor, ControllerListener {

    private ObjectMap<String, PlayerInputController> playerInputControllers;

    private static final ControlSettings instance;

    static {
        instance = new ControlSettings();
    }

    public ControlSettings() {
        this.playerInputControllers = new ObjectMap<>(16);
    }

    public static ControlSettings getInstance() {
        return instance;
    }

    public void resetInputState() {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.resetInputState();
        }
    }

    public boolean isDown(InputAction key, String player) {
        return playerInputControllers.get(player).isDown(key);
    }

    public boolean isPressed(InputAction key, String player) {
        return playerInputControllers.get(player).isPressed(key);
    }

    public boolean isReleased(InputAction key, String player) {
        return playerInputControllers.get(player).isReleased(key);
    }

    public void actFrame() {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.actFrame();
        }
    }

    public void setActionMapping(InputAction action, InputState mappedInput, String player) {
        playerInputControllers.get(player).setActionMapping(action, mappedInput);
    }

    public String getKeyName(InputAction action, String player, String unmappedString) {
        return playerInputControllers.get(player).getKeyName(action, unmappedString);
    }

    public boolean isAssigned(InputAction action, String player) {
        return playerInputControllers.get(player).isAssigned(action);
    }

    @Override
    public boolean keyDown(int keycode) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.keyDown(keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.keyUp(keycode);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void connected(Controller controller) {

    }

    @Override
    public void disconnected(Controller controller) {

    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.buttonDown(controller, buttonCode);
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.buttonUp(controller, buttonCode);
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.axisMoved(controller, axisCode, value);
        }
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        for (PlayerInputController playerInputController : playerInputControllers.values()) {
            playerInputController.povMoved(controller, povCode, value);
        }
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    public ObjectMap<String, PlayerInputController> getPlayerInputControllers() {
        return playerInputControllers;
    }
}
