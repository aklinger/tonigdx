package com.aa.tonigdx.dal.input;

import com.aa.tonigdx.dal.input.mappings.InputState;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;

import java.util.Map;

/**
 * Created by Toni on 07.12.2014.
 */
public class PlayerInputController implements Comparable<PlayerInputController> {

    private Map<InputAction, InputState> keyMap;
    private String name;

    private Controller assignedController;

    public PlayerInputController(String name, Map<InputAction, InputState> keyMap) {
        this.keyMap = keyMap;
        this.name = name;
    }

    public void resetInputState() {
        for (InputState keyMapping : keyMap.values()) {
            keyMapping.reset();
        }
    }

    public boolean isDown(InputAction key) {
        return keyMap.get(key).isDown();
    }

    public boolean isPressed(InputAction key) {
        return keyMap.get(key).isPressed();
    }

    public boolean isReleased(InputAction key) {
        return keyMap.get(key).isReleased();
    }

    public float getSliderValue(InputAction action) {
        return keyMap.get(action).getSliderValue();
    }

    public void actFrame() {
        for (InputState keyMapping : keyMap.values()) {
            keyMapping.actFrame();
        }
    }

    public void setActionMapping(InputAction action, InputState mappedInput) {
        keyMap.put(action, mappedInput);
    }

    public String getKeyName(InputAction action, String unmappedString) {
        if (isAssigned(action)) {
            return keyMap.get(action).getName();
        } else {
            if (unmappedString == null) {
                throw new RuntimeException("Action '" + action.getLabel() + "' is not assigned to a key.");
            } else {
                return unmappedString;
            }
        }
    }

    public InputState getAssignedInputState(InputAction action) {
        return keyMap.get(action);
    }

    public boolean isAssigned(InputAction action) {
        return keyMap.get(action) != null;
    }

    /*public void press(InputAction action) {
        keyMap.get(action).press();
    }
    public void release(InputAction action) {
        keyMap.get(action).release();
    }*/

    public boolean keyDown(int keycode) {
        for (InputState km : keyMap.values()) {
            km.updateWithKeyValue(keycode, true);
        }
        return false;
    }

    public boolean keyUp(int keycode) {
        for (InputState km : keyMap.values()) {
            km.updateWithKeyValue(keycode, false);
        }
        return false;
    }

    public boolean buttonDown(Controller controller, int buttonCode) {
        if (controller == assignedController) {
            for (InputState km : keyMap.values()) {
                km.updateWithButtonValue(buttonCode, true);
            }
        }
        return false;
    }

    public boolean buttonUp(Controller controller, int buttonCode) {
        if (controller == assignedController) {
            for (InputState km : keyMap.values()) {
                km.updateWithButtonValue(buttonCode, false);
            }
        }
        return false;
    }

    public boolean axisMoved(Controller controller, int axisCode, float value) {
        if (controller == assignedController) {
            for (InputState km : keyMap.values()) {
                km.updateWithAxisValue(axisCode, value);
            }
        }
        return false;
    }

    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        if (controller == assignedController) {
            for (InputState km : keyMap.values()) {
                km.updateWithPovValue(povCode, value);
            }
        }
        return false;
    }

    public void setAssignedController(Controller assignedController) {
        this.assignedController = assignedController;
    }

    public Controller getAssignedController() {
        return assignedController;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(PlayerInputController playerInputController) {
        return this.getName().compareTo(playerInputController.getName());
    }
}
