package com.aa.tonigdx.dal.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;

public class KeyCodeLogger extends InputAdapter {

    public KeyCodeLogger(boolean keyDown, boolean keyUp) {
        this.keyDown = keyDown;
        this.keyUp = keyUp;
    }

    private boolean keyDown;
    private boolean keyUp;

    @Override
    public boolean keyDown(int keycode) {
        if (keyDown) {
            Gdx.app.log("KeyCodeLogger", "Key down: " + keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keyUp) {
            Gdx.app.log("KeyCodeLogger", "Key up: " + keycode);
        }
        return false;
    }
}
