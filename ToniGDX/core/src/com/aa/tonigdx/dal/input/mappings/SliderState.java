package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.controllers.PovDirection;

/**
 * Created by Toni on 07.12.2014.
 */
public abstract class SliderState implements InputState {
    private float valueLastFrame;
    private float valueCurrentFrame;
    private boolean invert;
    private static final float threshold = 0.5f;
    public float deadzone = 0.05f;

    public SliderState(boolean invert) {
        this.invert = invert;
    }

    public void reset() {
        valueLastFrame = 0;
        valueCurrentFrame = 0;
    }

    public void actFrame() {
        valueLastFrame = valueCurrentFrame;
    }

    public boolean isDown() {
        return valueCurrentFrame > threshold;
    }

    private boolean wasDownLastFrame() {
        return (valueLastFrame) > threshold;
    }

    public boolean isPressed() {
        return isDown() && !(wasDownLastFrame());
    }

    public boolean isReleased() {
        return !isDown() && wasDownLastFrame();
    }

    @Override
    public float getSliderValue() {
        return valueCurrentFrame;
    }

    @Override
    public float getSliderVelocity() {
        return valueCurrentFrame - valueLastFrame;
    }

    public void setValue(float value) {
        if (invert) {
            value *= -1;
        }
        if (value < deadzone) {
            value = 0;
        } else {
            value = (value - deadzone) / (1 - deadzone);
        }
        this.valueCurrentFrame = value;
    }

    public boolean isInvert() {
        return invert;
    }

    //Abstract Stuff
    public void updateWithAxisValue(int axis, float value) {
    }

    public void updateWithKeyValue(int key, boolean pressed) {
    }

    public void updateWithButtonValue(int button, boolean pressed) {
    }

    public void updateWithPovValue(int povCode, PovDirection direction) {
    }

    public void setDeadzone(float deadzone) {
        this.deadzone = deadzone;
    }

    public float getDeadzone() {
        return deadzone;
    }
}
