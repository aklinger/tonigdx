package com.aa.tonigdx.dal.input.remap;

/**
 * Created by toni on 7/21/16.
 */
public interface RemapEventListener {
    boolean somethingRemapped(RemapEvent remapEvent);
}
