package com.aa.tonigdx.dal.input.mappings;

import java.util.Arrays;

/**
 * Created by Toni on 07.12.2014.
 */
public class MultiAxisMapping extends SliderState {
    private int[] axes;
    private float[] values;

    public MultiAxisMapping() {
        super(false);
    }

    public MultiAxisMapping(boolean invert, int... axes) {
        super(invert);
        this.axes = axes;
        values = new float[axes.length];
    }

    @Override
    public void updateWithAxisValue(int axis, float value) {
        float maxValue = 0;
        boolean update = false;
        for (int i = 0; i < axes.length; i++) {
            int axe = axes[i];
            if (axe == axis) {
                values[i] = value;
                update = true;
            }
            if (values[i] > maxValue ^ isInvert()) {
                maxValue = values[i];
            }
        }
        if (update) {
            setValue(maxValue);
        }
    }

    @Override
    public String getName() {
        return "Axis: " + (isInvert() ? "-" : "") + Arrays.toString(axes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MultiAxisMapping that = (MultiAxisMapping) o;

        return Arrays.equals(axes, that.axes) && isInvert() == that.isInvert();
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(axes);
    }

    public int[] getAxes() {
        return axes;
    }
}
