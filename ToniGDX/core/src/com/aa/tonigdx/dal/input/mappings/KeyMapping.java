package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.Input;

/**
 * Created by Toni on 12.07.2014.
 */
public class KeyMapping extends BooleanState {
    private int key;

    public KeyMapping() {
    }

    public KeyMapping(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    @Override
    public void updateWithKeyValue(int key, boolean pressed) {
        if (this.key == key) {
            if (pressed) {
                press();
            } else {
                release();
            }
        }
    }

    @Override
    public String getName() {
        return Input.Keys.toString(key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyMapping that = (KeyMapping) o;

        return key == that.key;
    }

    @Override
    public int hashCode() {
        return key;
    }
}
