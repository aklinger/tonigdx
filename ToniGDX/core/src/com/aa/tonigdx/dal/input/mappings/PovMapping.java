package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.controllers.PovDirection;

/**
 * Created by toni on 8/6/16.
 */
public class PovMapping extends BooleanState {

    private int povCode;
    private PovDirection mappingDirection;

    private static final PovDirection[] POV_WHEEL = {
            PovDirection.north, PovDirection.northEast,
            PovDirection.east, PovDirection.southEast,
            PovDirection.south, PovDirection.southWest,
            PovDirection.west, PovDirection.northWest};

    public PovMapping() {
    }

    public PovMapping(int povCode, PovDirection mappingDirection) {
        this.povCode = povCode;
        this.mappingDirection = mappingDirection;
    }

    @Override
    public void updateWithPovValue(int povCode, PovDirection direction) {
        if (this.povCode == povCode) {
            if (isNearToDirection(this.mappingDirection, direction)) {
                press();
            } else {
                release();
            }
        }
    }

    public boolean isNearToDirection(PovDirection base, PovDirection target) {
        if (target.equals(PovDirection.center)) {
            return false;
        }
        int baseIndex = 0;
        int targetIndex = 0;
        for (int i = 0; i < POV_WHEEL.length; i++) {
            if (POV_WHEEL[i].equals(base)) {
                baseIndex = i;
            }
            if (POV_WHEEL[i].equals(target)) {
                targetIndex = i;
            }
        }
        int delta = targetIndex - baseIndex;
        if (delta > 4) {
            delta -= 8;
        } else if (delta < -4) {
            delta += 8;
        }
        return delta >= -1 && delta <= 1;
    }

    @Override
    public String getName() {
        return "POV_" + povCode + ":" + mappingDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PovMapping that = (PovMapping) o;

        return povCode == that.povCode && mappingDirection == that.mappingDirection;
    }

    @Override
    public int hashCode() {
        int result = povCode;
        result = 31 * result + (mappingDirection != null ? mappingDirection.hashCode() : 0);
        return result;
    }
}
