package com.aa.tonigdx.dal.input;

/**
 * Created by Toni on 15.04.2015.
 */
public interface InputAction {
    public String getCode();
    public String getLabel();
}
