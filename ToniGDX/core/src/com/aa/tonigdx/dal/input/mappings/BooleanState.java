package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.controllers.PovDirection;

/**
 * Created by Toni on 12.07.2014.
 */
public abstract class BooleanState implements InputState {
    private boolean downLastFrame;
    private boolean downCurrentFrame;
    private boolean pressedLastFrame;
    private boolean pressedCurrentFrame;
    private boolean releasedLastFrame;
    private boolean releasedCurrentFrame;

    @Override
    public void reset() {
        downLastFrame = false;
        downCurrentFrame = false;
        pressedLastFrame = false;
        pressedCurrentFrame = false;
        releasedLastFrame = false;
        releasedCurrentFrame = false;
    }

    public void press() {
        pressedCurrentFrame = true;
        downCurrentFrame = true;
    }

    public void release() {
        releasedCurrentFrame = true;
        downCurrentFrame = false;
    }

    public void actFrame() {
        releasedLastFrame = releasedCurrentFrame;
        pressedLastFrame = pressedCurrentFrame;
        downLastFrame = downCurrentFrame;
        pressedCurrentFrame = false;
        releasedCurrentFrame = false;
    }

    public boolean isDown() {
        return downLastFrame;
    }

    public boolean isPressed() {
        return pressedLastFrame;
    }

    public boolean isReleased() {
        return releasedLastFrame;
    }

    @Override
    public float getSliderValue() {
        return downLastFrame ? 1 : 0;
    }

    @Override
    public float getSliderVelocity() {
        return pressedLastFrame ? 1 : (releasedLastFrame ? -1 : 0);
    }

    //Abstract Stuff
    public void updateWithAxisValue(int axis, float value) {
    }

    public void updateWithKeyValue(int key, boolean pressed) {
    }

    public void updateWithButtonValue(int button, boolean pressed) {
    }

    public void updateWithPovValue(int povCode, PovDirection direction) {
    }
}
