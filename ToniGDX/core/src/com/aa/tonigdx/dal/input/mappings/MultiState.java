package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.utils.Array;

import java.util.Objects;

/**
 * Created by toni on 6/22/16.
 */
public class MultiState implements InputState {

    private Array<InputState> inputStateList;

    public MultiState() {
        this.inputStateList = new Array<>();
    }

    public MultiState(InputState... inputStateList) {
        this.inputStateList = Array.with(inputStateList);
    }

    @Override
    public void reset() {
        for (InputState inputState : inputStateList) {
            inputState.reset();
        }
    }

    @Override
    public void actFrame() {
        for (InputState inputState : inputStateList) {
            inputState.actFrame();
        }
    }

    @Override
    public boolean isDown() {
        for (InputState inputState : inputStateList) {
            if (inputState.isDown()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isPressed() {
        boolean anyPressed = false;
        for (InputState inputState : inputStateList) {
            if (inputState.isDown()) {
                if (inputState.isPressed()) {
                    anyPressed = true;
                } else {
                    return false;
                }
            }
        }
        return anyPressed;
    }

    @Override
    public boolean isReleased() {
        boolean anyReleased = false;
        for (InputState inputState : inputStateList) {
            if (inputState.isDown()) {
                return false;
            } else {
                if (inputState.isReleased()) {
                    anyReleased = true;
                }
            }
        }
        return anyReleased;
    }

    @Override
    public float getSliderValue() {
        float maxValue = 0;
        for (InputState inputState : inputStateList) {
            maxValue = Math.max(maxValue, inputState.getSliderValue());
        }
        return maxValue;
    }

    @Override
    public float getSliderVelocity() {
        return 0;//FIXME: this is not well defined in a multi button/axis setup
    }

    @Override
    public void updateWithAxisValue(int axis, float value) {
        for (InputState inputState : inputStateList) {
            inputState.updateWithAxisValue(axis, value);
        }
    }

    @Override
    public void updateWithKeyValue(int key, boolean pressed) {
        for (InputState inputState : inputStateList) {
            inputState.updateWithKeyValue(key, pressed);
        }
    }

    @Override
    public void updateWithButtonValue(int button, boolean pressed) {
        for (InputState inputState : inputStateList) {
            inputState.updateWithButtonValue(button, pressed);
        }
    }

    @Override
    public void updateWithPovValue(int povCode, PovDirection direction) {
        for (InputState inputState : inputStateList) {
            inputState.updateWithPovValue(povCode, direction);
        }
    }

    @Override
    public String getName() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < inputStateList.size; i++) {
            InputState inputState = inputStateList.get(i);
            sb.append(inputState.getName());
            if (i != inputStateList.size - 1) {
                sb.append(";");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public Array<InputState> getInputStateList() {
        return inputStateList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MultiState that = (MultiState) o;
        return Objects.equals(inputStateList, that.inputStateList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inputStateList);
    }
}
