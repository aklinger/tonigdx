package com.aa.tonigdx.dal.input.mappings;

/**
 * Created by Toni on 07.12.2014.
 */
public class ButtonMapping extends BooleanState {
    private int button;

    public ButtonMapping() {
    }

    public ButtonMapping(int button) {
        this.button = button;
    }

    public int getButton() {
        return button;
    }

    @Override
    public void updateWithButtonValue(int button, boolean pressed) {
        if (this.button == button) {
            if (pressed) {
                press();
            } else {
                release();
            }
        }
    }

    @Override
    public String getName() {
        return "Button: " + button;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ButtonMapping that = (ButtonMapping) o;

        return button == that.button;
    }

    @Override
    public int hashCode() {
        return button;
    }
}
