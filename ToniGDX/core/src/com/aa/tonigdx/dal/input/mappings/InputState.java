package com.aa.tonigdx.dal.input.mappings;

import com.badlogic.gdx.controllers.PovDirection;

/**
 * Created by Toni on 07.12.2014.
 */
public interface InputState {
    void reset();
    void actFrame();
    boolean isDown();
    boolean isPressed();
    boolean isReleased();
    float getSliderValue();
    float getSliderVelocity();

    void updateWithAxisValue(int axis, float value);
    void updateWithKeyValue(int key, boolean pressed);
    void updateWithButtonValue(int button, boolean pressed);
    void updateWithPovValue(int povCode, PovDirection direction);

    String getName();
}
