package com.aa.tonigdx.dal.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;

public class GamepadLogger extends ControllerAdapter {
    @Override
    public void connected(Controller controller) {
        Gdx.app.log("GamepadLogger", "Connected controller: " + controller.getName());
    }

    @Override
    public void disconnected(Controller controller) {
        Gdx.app.log("GamepadLogger", "Disconnected controller: " + controller.getName());
    }
}
