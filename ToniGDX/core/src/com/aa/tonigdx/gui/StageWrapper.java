package com.aa.tonigdx.gui;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Toni on 10.05.2015.
 */
public class StageWrapper{
    private Stage stage;
    private boolean active;
    private TweenTransition enterTransition;
    private TweenTransition exitTransition;

    public StageWrapper(Stage stage) {
        this.stage = stage;
        active = true;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public TweenTransition getEnterTransition() {
        return enterTransition;
    }

    public void setEnterTransition(TweenTransition enterTransition) {
        this.enterTransition = enterTransition;
    }

    public TweenTransition getExitTransition() {
        return exitTransition;
    }

    public void setExitTransition(TweenTransition exitTransition) {
        this.exitTransition = exitTransition;
    }
}
