package com.aa.tonigdx.gui;

import com.aa.tonigdx.util.LogUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * The base class for all game screens.
 * <p/>
 * Based on: http://steigert.blogspot.co.at/2012/02/3-libgdx-tutorial-scene2d.html
 * <p/>
 *
 * @author Steigert
 */
public abstract class AbstractScreen implements Screen, InputProcessor {
    protected final Stage stage;
    private final String screenName;
    private Color clearColor = Color.WHITE;

    public boolean useStage = true;

    public AbstractScreen(String screenName, Batch batch) {
        this.screenName = screenName;
        this.stage = new Stage(new FillViewport(0, 0), batch);
    }

    protected String getScreenName() {
        return screenName;
    }

    // Screen implementation
    @Override
    public void show() {
        LogUtil.log("Showing screen: " + getScreenName());

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(this);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void resize(int width, int height) {
        LogUtil.log("Resizing screen: " + getScreenName() + " to: " + width + " x " + height);

        // resize the stage
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(float delta) {
        // the following code clears the screen with the given RGB color 
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // update and draw the stage actors
        if (useStage) {
            stage.act(delta);
            stage.draw();
        }
    }

    @Override
    public void hide() {
        LogUtil.log("Hiding screen: " + getScreenName());
    }

    @Override
    public void pause() {
        LogUtil.log("Pausing screen: " + getScreenName());
    }

    @Override
    public void resume() {
        LogUtil.log("Resuming screen: " + getScreenName());
    }

    @Override
    public void dispose() {
        LogUtil.log("Disposing screen: " + getScreenName());

        // dispose the collaborators
        stage.dispose();
    }

    public void setClearColor(Color clearColor) {
        this.clearColor = clearColor;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
