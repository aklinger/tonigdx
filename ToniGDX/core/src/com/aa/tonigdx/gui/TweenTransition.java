package com.aa.tonigdx.gui;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquation;
import com.aa.tonigdx.logic.tween.ActorAccessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Toni on 11.05.2015.
 */
public abstract class TweenTransition {
    public abstract Timeline getTransition();

    static class BasicExit extends TweenTransition{

        private Actor actor;
        private float time;
        private Vector2 displacement;
        private TweenEquation tweenEquation;

        public BasicExit(Actor actor, float time, Vector2 displacement, TweenEquation tweenEquation) {
            this.actor = actor;
            this.time = time;
            this.displacement = displacement;
            this.tweenEquation = tweenEquation;
        }

        @Override
        public Timeline getTransition() {
            return Timeline.createSequence()
                    .push(Tween.to(actor, ActorAccessor.POSITION_XY, time).targetRelative(displacement.x, displacement.y).ease(tweenEquation))
                    .push(Tween.set(actor, ActorAccessor.POSITION_XY).targetRelative(-displacement.x, -displacement.y));
        }
    }

    static class BasicEnter extends TweenTransition{

        private Actor actor;
        private float time;
        private Vector2 displacement;
        private TweenEquation tweenEquation;

        public BasicEnter(Actor actor, float time, Vector2 displacement, TweenEquation tweenEquation) {
            this.actor = actor;
            this.time = time;
            this.displacement = displacement;
            this.tweenEquation = tweenEquation;
        }

        @Override
        public Timeline getTransition() {
            return Timeline.createSequence()
                    .push(Tween.from(actor, ActorAccessor.POSITION_XY, time).targetRelative(displacement.x,displacement.y).ease(tweenEquation));
        }
    }
}
