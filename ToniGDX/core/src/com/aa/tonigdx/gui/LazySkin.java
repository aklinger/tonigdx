package com.aa.tonigdx.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by toni on 4/17/16.
 */
public class LazySkin extends Skin {

    private static final String defaultName = "default";

    public LazySkin(FileHandle skinFile) {
        super(skinFile);
    }

    @Override
    public Drawable getDrawable(String name) {
        try {
            return super.getDrawable(name);
        }catch(GdxRuntimeException ex){
            Gdx.app.log("LazySkin", "Couldn't load "+name+". LazySkin loads default instead.");
            return super.getDrawable(defaultName);
        }
    }
}
