package com.aa.tonigdx.gui;

import aurelienribon.tweenengine.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.TreeMap;

/**
 * Created by Toni on 08.05.2015.
 */
public class ScreenMultiplexer implements InputProcessor {

    private TreeMap<String, StageWrapper> stages;
    private TweenManager tweenManager;
    private boolean transition;

    public ScreenMultiplexer() {
        stages = new TreeMap<>();
        tweenManager = new TweenManager();
    }

    public void putStage(String key, Stage stage) {
        stages.put(key, new StageWrapper(stage));
    }

    public void removeStage(String key) {
        stages.remove(key);
    }

    public Stage getStage(String key) {
        return stages.get(key).getStage();
    }

    public void setActive(String key, boolean active) {
        stages.get(key).setActive(active);
    }

    public boolean isActive(String key) {
        return stages.get(key).isActive();
    }

    public void addTransition(String key, TweenTransition tweenTransition, boolean enter) {
        if (enter) {
            stages.get(key).setEnterTransition(tweenTransition);
        } else {
            stages.get(key).setExitTransition(tweenTransition);
        }
    }

    public TweenTransition getTransition(String key, boolean enter) {
        if (enter) {
            return stages.get(key).getEnterTransition();
        } else {
            return stages.get(key).getExitTransition();
        }
    }

    public void actStages() {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive()) {
                stage.getStage().act();
            }
        }
        tweenManager.update(Gdx.graphics.getDeltaTime());
    }

    public void renderStages() {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive()) {
                stage.getStage().draw();
            }
        }
    }

    public boolean keyDown(int keycode) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().keyDown(keycode)) return true;
        }
        return false;
    }

    public boolean keyUp(int keycode) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().keyDown(keycode)) return true;
        }
        return false;
    }

    public boolean keyTyped(char character) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().keyTyped(character)) return true;
        }
        return false;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().touchDown(screenX, screenY, pointer, button)) return true;
        }
        return false;
    }

    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().touchUp(screenX, screenY, pointer, button)) return true;
        }
        return false;
    }

    public boolean touchDragged(int screenX, int screenY, int pointer) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().touchDragged(screenX, screenY, pointer)) return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().mouseMoved(screenX, screenY)) return true;
        }
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        for (StageWrapper stage : stages.values()) {
            if (stage.isActive() && stage.getStage().scrolled(amount)) return true;
        }
        return false;
    }

    public void updateViewportSize(int screenWidth, int screenHeight) {
        for (StageWrapper stageWrapper : stages.values()) {
            stageWrapper.getStage().getViewport().update(screenWidth, screenHeight);
        }
    }

    public TweenManager getTweenManager() {
        return tweenManager;
    }

    public boolean isTransition() {
        return transition;
    }

    public void setTransition(boolean transition) {
        this.transition = transition;
    }

    protected final TweenCallback START_TRANSITION = new TweenCallback() {
        @Override
        public void onEvent(int type, BaseTween<?> source) {
            transition = true;
        }
    };
    protected final TweenCallback END_TRANSITION = new TweenCallback() {
        @Override
        public void onEvent(int type, BaseTween<?> source) {
            transition = false;
        }
    };

    protected class ToggleScreen implements TweenCallback {
        private String screen;
        private boolean active;

        public ToggleScreen(String screen, boolean active) {
            this.screen = screen;
            this.active = active;
        }

        @Override
        public void onEvent(int type, BaseTween<?> source) {
            setActive(screen, active);
        }
    }

    public void enterScreenTween(String screenKey, TweenCallback... callbacks) {
        Timeline timeline = Timeline.createSequence()
                .push(Tween.call(START_TRANSITION))
                .push(Tween.call(new ToggleScreen(screenKey, true)));
        TweenTransition tweenTransition = getTransition(screenKey, true);
        if (tweenTransition != null) {
            timeline.push(tweenTransition.getTransition());
        }
        for (TweenCallback callback : callbacks) {
            timeline.push(Tween.call(callback));
        }
        timeline.push(Tween.call(END_TRANSITION))
                .start(getTweenManager());
    }

    public void exitScreenTween(String screenKey, TweenCallback... callbacks) {
        Timeline timeline = Timeline.createSequence()
                .push(Tween.call(START_TRANSITION));
        TweenTransition tweenTransition = getTransition(screenKey, false);
        if (tweenTransition != null) {
            timeline.push(tweenTransition.getTransition());
        }
        timeline.push(Tween.call(new ToggleScreen(screenKey, false)));
        for (TweenCallback callback : callbacks) {
            timeline.push(Tween.call(callback));
        }
        timeline.push(Tween.call(END_TRANSITION))
                .start(getTweenManager());
    }

    public void transitionBetweenScreens(String exitScreen, String enterScreen, TweenCallback... callbacks) {
        Timeline timeline = Timeline.createSequence()
                .push(Tween.call(START_TRANSITION))
                .push(getTransition(exitScreen, false).getTransition())
                .push(Tween.call(new ToggleScreen(exitScreen, false)))
                .push(Tween.call(new ToggleScreen(enterScreen, true)))
                .push(getTransition(enterScreen, true).getTransition());
        for (TweenCallback callback : callbacks) {
            timeline.push(Tween.call(callback));
        }
        timeline.push(Tween.call(END_TRANSITION))
                .start(getTweenManager());
    }
}
