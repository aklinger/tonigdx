package com.aa.tonigdx.map;

/**
 * Represents a Hexagonal map.
 * <p/>
 * (Flat-Topped) (Axial coordinates)
 * <p/>
 *
 * @author Toni
 */
public class HexagonMap<T extends Object> {
    //Attributes
    private T[][] data;
    private float size;//radius
    //Constants
    private static final double SQRT_3 = Math.sqrt(3);

    //Directional Constants
    public enum Direction {
        UP(0, 1),
        RIGHT_UP(1, 0),
        RIGHT_DOWN(1, -1),
        DOWN(0, -1),
        LEFT_DOWN(-1, 0),
        LEFT_UP(-1, 1);
        //Attributes
        private int x;
        private int y;

        //Constructor
        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Direction turnAround(boolean clockwise) {
            int ordinal = this.ordinal();
            ordinal -= clockwise ? 1 : -1;
            if (ordinal < 0) {
                ordinal = Direction.values().length - 1;
            }else if(ordinal >= Direction.values().length){
                ordinal = 0;
            }
            return Direction.values()[ordinal];
        }

        //Getter & Setter
        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    //Constructors
    public HexagonMap(int width, int height) {
        data = (T[][]) new Object[width][height];
    }

    //Methods
    public void setTile(int x, int y, T tile) {
        data[x][y] = tile;
    }

    public T getTile(int x, int y) {
        if (checkRange(x, y)) {
            return data[x][y];
        } else {
            return null;
        }
    }

    public boolean checkRange(int x, int y) {
        return x >= 0 && x < getXCount() && y >= 0 && y < getYCount();
    }

    public T getNeighbor(int x, int y, Direction direction) {
        return getTile(x + direction.x, y + direction.y);
    }

    public T[] getNeighbors(int x, int y) {
        T[] neighbors = (T[]) new Object[Direction.values().length];
        for (int i = 0; i < Direction.values().length; i++) {
            neighbors[i] = getTile(x + Direction.values()[i].x, y + Direction.values()[i].y);
        }
        return neighbors;
    }

    public float[] hexToPixel(int x, int y) {
        return new float[]{(float) (size * 3f / 2f * x), (float) (size * SQRT_3 * (y + x / 2f))};
    }

    public int[] pixelToHex(int x, int y) {
        x -= size;
        y += size;

        float q = 2f / 3f * x / size;
        float r = (float) ((1f / 3f * SQRT_3 * y - 1f / 3f * x) / size);

        return roundToNearestHex(q, r, -q - r);
    }

    private int[] roundToNearestHex(float x, float y, float z) {
        int rx = (int) Math.round(x);
        int ry = (int) Math.round(y);
        int rz = (int) Math.round(z);

        double x_diff = Math.abs(rx - x);
        double y_diff = Math.abs(ry - y);
        double z_diff = Math.abs(rz - z);

        if (x_diff > y_diff && x_diff > z_diff) {
            rx = -ry - rz;
        } else if (!(y_diff > z_diff)) {
            rz = -rx - ry;
        }
        return new int[]{rx, rz};
    }

    //Getter & Setter
    public int getXCount() {
        return data.length;
    }

    public int getYCount() {
        return data[0].length;
    }

    public void setTileSize(float tileSize) {
        this.size = tileSize;
    }

    public float getTileSize() {
        return size;
    }
}
