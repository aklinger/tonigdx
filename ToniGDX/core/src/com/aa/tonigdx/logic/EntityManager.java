package com.aa.tonigdx.logic;

import com.aa.tonigdx.logic.camera.CameraManager;
import com.aa.tonigdx.logic.camera.ScreenShake;
import com.aa.tonigdx.logic.event.GameEventProvider;
import com.aa.tonigdx.logic.particle.Particle;
import com.aa.tonigdx.logic.particle.ParticleList;
import com.aa.tonigdx.logic.particle.ParticleManager;
import com.aa.tonigdx.logic.physics.BasicCollisionResolver;
import com.aa.tonigdx.logic.physics.CollisionResolver;
import com.aa.tonigdx.logic.physics.SimpleCollisionResolver;
import com.aa.tonigdx.logic.tile.TileManager;
import com.aa.tonigdx.maths.Collision;
import com.aa.tonigdx.maths.CollisionUtil;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Manages all entities that are in the game at any given moment.
 *
 * @author Toni
 */
public class EntityManager extends GameEventProvider {

    private Array<Entity> entities;
    private Array<Entity> inbox;
    private Array<Entity> trash;
    private ParticleManager particles;
    private TileManager tileManager;

    private int gameAreaWidth;
    private int gameAreaHeight;

    private CameraManager cameraManager;
    protected CollisionResolver collisionResolver;

    public EntityManager(CameraManager cameraManager) {
        this((int) cameraManager.getCamera().viewportWidth, (int) cameraManager.getCamera().viewportHeight, cameraManager);
    }

    public EntityManager(int gameAreaWidth, int gameAreaHeight, CameraManager cameraManager) {
        entities = new Array<>();
        inbox = new Array<>();
        trash = new Array<>();
        particles = new ParticleList();

        this.gameAreaWidth = gameAreaWidth;
        this.gameAreaHeight = gameAreaHeight;
        this.cameraManager = cameraManager;
        this.collisionResolver = new BasicCollisionResolver();
    }

    public void initSomeStuff() {
        cameraManager.update();
        addInbox();
    }

    public void act() {
        addInbox();
        actEntities();
        removeTrash();

        actParticles();

        cameraManager.update();
    }

    protected void actParticles() {
        particles.act();
    }

    protected void actEntities() {
        for (int i = 0; i < entities.size; i++) {
            Entity ent1 = entities.get(i);
            ent1.act(this);
            for (int j = i + 1; j < entities.size; j++) {
                Entity ent2 = entities.get(j);
                Collision col = null;
                if (Entity.canCollideWith(ent1, ent2)) {
                    col = CollisionUtil.calcCollisionHitBoxes(ent1, ent2, true, false);
                    if (Entity.willResolveCollision(ent1, ent2) && col != null) {
                        collisionResolver.resolveCollision(col, !(ent1.isPushProtection() || ent2.isPushProtection()));
                    }
                }
                ent1.interactWith(ent2, col, this);
                ent2.interactWith(ent1, col, this);
            }
            handleTileManagerCollision(ent1);
            if (!ent1.isAlive()) {
                killEntity(ent1, true);
            }
        }
        for (Entity ent1 : entities) {
            ent1.afterAct(this);
        }
    }

    protected void handleTileManagerCollision(Entity entity) {
        if (tileManager != null) {
            tileManager.checkCollision(entity, this, collisionResolver, null);
        }
    }

    public Vector2 moveForcefully(Entity moved, Vector2 delta, Entity pushedBy) {
        List<Entity> ridingList = new ArrayList<>();
        for (Entity entity : entities) {
            if ((entity).isRiding(moved)) {
                ridingList.add(entity);
            }
        }
        Vector2 positionDelta = moved.getCenter();

        moved.addPosition(delta);

        if (tileManager != null) {
            tileManager.checkCollision(moved, this, collisionResolver, pushedBy);
        }
        positionDelta = moved.getCenter().sub(positionDelta);

        for (Entity entity : entities) {
            if (entity != moved && entity != pushedBy) {
                Collision collision = CollisionUtil.calcCollisionHitBoxes(entity, moved, true, false);
                if (collision != null) {
                    ridingList.remove(entity);
                    moved.interactWith(entity, collision, this);
                    if (Entity.willResolveCollision(moved, entity)) {
                        if (moved.canPush(entity)) {
                            Vector2 pushedDist = collision.getDisplacementRelativeTo(entity);
                            safelyMoveEntity(entity, pushedDist, moved);
                        } else {
                            Vector2 pushedDist = collision.getDisplacementRelativeTo(entity);
                            Vector2 pushedDelta = moveForcefully(entity, pushedDist, moved);
                            pushedDelta.sub(pushedDist);
                            moved.addPosition(pushedDelta);
                            positionDelta.add(pushedDelta);
                        }
                    }
                }
            }
        }

        for (Entity entity : ridingList) {
            this.safelyMoveEntity(entity, positionDelta, null);
        }

        return positionDelta;
    }

    public void safelyMoveEntity(Entity entity, Vector2 delta, Entity pushedBy) {
        entity.addPosition(delta);
        for (Entity other : entities) {
            if (other != pushedBy && entity != other) {
                Collision col = CollisionUtil.calcCollisionHitBoxes(entity, other, true, false);
                if (col != null) {
                    entity.interactWith(other, col, this);
                    if (Entity.willResolveCollision(other, entity)) {
                        if (pushedBy != null) {
                            entity.squish(this, col, pushedBy);
                        }
                        col.setMain(entity);
                        collisionResolver.resolveCollision(col, false);
                    }
                }
            }
        }
        if (tileManager != null) {
            tileManager.checkCollision(entity, this, collisionResolver, pushedBy);
        }
    }

    public Vector2 moveForcefullyVersion2(Entity moved, Vector2 delta, Entity pushedBy) {
        HashSet<Entity> pushers = new HashSet<>();
        if (pushedBy != null) {
            pushers.add(pushedBy);
        }
        return moveForcefullyVersion2(moved, delta, pushedBy, pushers);
    }

    private Vector2 moveForcefullyVersion2(Entity moved, Vector2 delta, Entity pushedBy, Set<Entity> pushers) {
        pushers.add(moved);
        List<Entity> ridingList = new ArrayList<>();
        for (Entity entity : entities) {
            if ((entity).isRiding(moved) && !pushers.contains(entity)) {
                ridingList.add(entity);
            }
        }
        Vector2 positionDelta = moved.getCenter();

        moved.addPosition(delta);

        if (tileManager != null) {
            tileManager.checkCollision(moved, this, collisionResolver, pushedBy);
        }
        positionDelta = moved.getCenter().sub(positionDelta);

        for (Entity entity : entities) {
            if (!pushers.contains(entity) && entity.willResolveCollisionWith(moved) && moved.willResolveCollisionWith(entity)) {
                Collision collision = CollisionUtil.calcCollisionHitBoxes(entity, moved, true, false);
                if (collision != null) {
                    ridingList.remove(entity);
                    if (moved.canPush(entity)) {
                        Vector2 pushedDist = collision.getDisplacementRelativeTo(entity);
                        Vector2 pushedDelta = moveForcefullyVersion2(entity, pushedDist, moved, pushers);
                        pushedDelta.sub(pushedDist);
                        if (!pushedDelta.isZero()) {
                            moved.interactWith(entity, collision, this);
                            if (pushedBy != null) {
                                moved.squish(this, collision, pushedBy);
                            }
                            SimpleCollisionResolver.stopVelocity(moved, pushedDelta);
                            moved.addPosition(pushedDelta);
                            positionDelta.add(pushedDelta);
                        }
                    } else {
                        moved.interactWith(entity, collision, this);
                        if (pushedBy != null) {
                            moved.squish(this, collision, pushedBy);
                        }
                        collision.setMain(moved);
                        collisionResolver.resolveCollision(collision, false);
                        Vector2 displacement = collision.getDisplacementRelativeTo(moved);
                        positionDelta.add(displacement);
                    }
                }
            }
        }

        for (Entity entity : ridingList) {
            this.safelyMoveEntity(entity, positionDelta, null);
        }

        return positionDelta;
    }

    protected void addInbox() {
        if (inbox.size > 0) {
            addEntities(inbox);
            Array<Entity> spawned = new Array<>(inbox);
            inbox.clear();
            for (Entity entity : spawned) {
                entity.onSpawn(this);
            }
        }
    }

    protected void removeTrash() {
        if (trash.size > 0) {
            removeEntities(trash);
            trash.clear();
        }
    }

    public void render(Batch batch) {
        for (Entity ent : entities) {
            if (!ent.isForeground()) {
                ent.render(batch);
            }
        }
        particles.render(batch);
        for (Entity ent : entities) {
            if (ent.isForeground()) {
                ent.render(batch);
            }
        }
    }

    public void debugRender(ShapeRenderer renderer) {
        for (Entity ent : entities) {
            ent.debugRender(renderer);
        }
    }

    protected void addEntities(Array<Entity> ents) {
        entities.addAll(ents);
    }

    protected void removeEntities(Array<Entity> ents) {
        entities.removeAll(ents, true);
    }

    public void clearAllEntities() {
        cleanupLevel();
        inbox.clear();
        entities.clear();
        trash.clear();
    }

    public void spawnEntity(Entity ent) {
        if (!inbox.contains(ent, true)) {
            inbox.add(ent);
        }
    }

    public void killEntity(Entity ent, boolean removeWithDeathTrigger) {
        if (removeWithDeathTrigger) {
            ent.die(this);
        }
        trash.add(ent);
    }

    public void cleanupLevel() {
        for (Entity entity : entities) {
            entity.cleanup(this);
        }
    }

    public Vector2 toInGameCoordinates(int screenX, int screenY) {
        return cameraManager.toInGameCoordinates(screenX, screenY);
    }

    public void addFancyStuff(Particle p) {
        particles.addParticle(p);
    }

    public ParticleManager getParticles() {
        return particles;
    }

    public Array<Entity> getEntities() {
        return entities;
    }

    public OrthographicCamera getCamera() {
        return cameraManager.getCamera();
    }

    public void setScreenSize(int screenWidth, int screenHeight) {
        cameraManager.setScreenSize(screenWidth, screenHeight);
    }

    public int getGameAreaWidth() {
        return gameAreaWidth;
    }

    public int getGameAreaHeight() {
        return gameAreaHeight;
    }

    public void setGameAreaWidth(int gameAreaWidth) {
        this.gameAreaWidth = gameAreaWidth;
    }

    public void setGameAreaHeight(int gameAreaHeight) {
        this.gameAreaHeight = gameAreaHeight;
    }

    public void setTileManager(TileManager tileManager) {
        this.tileManager = tileManager;
    }

    public TileManager getTileManager() {
        return tileManager;
    }

    public ScreenShake getScreenShake() {
        return cameraManager.getScreenShake();
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    public void setCollisionResolver(CollisionResolver collisionResolver) {
        this.collisionResolver = collisionResolver;
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    protected Array<Entity> getInbox() {
        return inbox;
    }

    protected Array<Entity> getTrash() {
        return trash;
    }
}
