package com.aa.tonigdx.logic;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by Toni on 25.01.2016.
 */
public abstract class GameManager extends InputAdapter {

    public abstract void actFrame();

    public abstract void render(Batch batch);

    public abstract void startLevel(String levelId);

    public abstract void resetLevel();

    public abstract void resize(int width, int height);
}
