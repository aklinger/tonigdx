package com.aa.tonigdx.logic.event;

/**
 * Created by toni on 6/22/16.
 */
public interface GameEventListener {
    void processGameEvent(GameEvent gameEvent, Object data, Object sender);
}
