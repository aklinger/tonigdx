package com.aa.tonigdx.logic.event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by toni on 6/22/16.
 */
public class GameEventProvider {

    private List<GameEventListener> listeners;

    public GameEventProvider() {
        listeners = new ArrayList<>();
    }

    public void sendEvent(GameEvent gameEvent, Object data){
        sendEvent(gameEvent, data, this);
    }

    public void sendEvent(GameEvent gameEvent, Object data, Object sender){
        for (GameEventListener listener : listeners) {
            listener.processGameEvent(gameEvent, data, sender);
        }
    }

    public void addListener(GameEventListener listener){
        listeners.add(listener);
    }

    public void removeListener(GameEventListener listener){
        listeners.remove(listener);
    }

    public void clearListeners(){
        listeners.clear();
    }
}
