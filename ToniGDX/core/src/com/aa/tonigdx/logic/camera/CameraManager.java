package com.aa.tonigdx.logic.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by toni on 6/21/16.
 */
public class CameraManager {

    protected Vector2 cameraFocus;
    private float smoothing;
    private final Vector2 bounds = new Vector2();
    private final Vector2 lowerbounds = new Vector2();
    private boolean boundsEnabled;
    private OrthographicCamera camera;
    private ScreenShake screenShake;
    private boolean keepAtPixels;

    public static CameraManager createDefaultCameraManager(int viewportWidth, int viewportHeight) {
        ScreenShake screenShake = new ScreenShake(1, 0.99f);
        OrthographicCamera camera = new OrthographicCamera(viewportWidth, viewportHeight);
        return new CameraManager(camera, screenShake, 0.5f);
    }

    public CameraManager(OrthographicCamera camera, ScreenShake screenShake, float smoothing) {
        this.camera = camera;
        this.screenShake = screenShake;
        this.smoothing = smoothing;

        this.boundsEnabled = false;
        this.cameraFocus = new Vector2(camera.viewportWidth / 2, camera.viewportHeight / 2);
    }

    public void update() {
        updateScreenshake();
        updateCamera();
    }

    private void updateScreenshake() {
        screenShake.update();
    }

    private void updateCamera() {
        moveTo(cameraFocus, smoothing);
        if (boundsEnabled && (!lowerbounds.isZero() || !bounds.isZero())) {
            keepInBounds(lowerbounds.x, lowerbounds.y, bounds.x, bounds.y);
        }
        camera.translate(screenShake.getScreenDisplacementX(), screenShake.getScreenDisplacementY());
        if (keepAtPixels) {
            camera.position.x = (int) camera.position.x;
            camera.position.y = (int) camera.position.y;
            camera.position.z = (int) camera.position.z;
        }
        camera.update();
    }

    private Vector3 moveTo = new Vector3();

    public void moveTo(Vector2 position, float alpha) {
        moveTo.set(position, 0);
        camera.position.lerp(moveTo, alpha);
    }

    protected void keepInBounds(float leftBound, float lowerBound, float rightBound, float upperBound) {
        OrthographicCamera camera = getCamera();
        float realWidth = camera.viewportWidth * camera.zoom;
        float realHeight = camera.viewportHeight * camera.zoom;

        float boundWidth = rightBound - leftBound;
        float boundHeight = upperBound - lowerBound;

        leftBound += realWidth / 2;
        rightBound -= realWidth / 2;
        lowerBound += realHeight / 2;
        upperBound -= realHeight / 2;

        if (realWidth > boundWidth) {
            camera.position.x = (leftBound + rightBound) / 2;
            leftBound = rightBound = camera.position.x;
        } else if (camera.position.x < leftBound) {
            camera.position.x = leftBound;
        } else if (camera.position.x > rightBound) {
            camera.position.x = rightBound;
        }
        if (realHeight > boundHeight) {
            camera.position.y = (lowerBound + upperBound) / 2;
            lowerBound = upperBound = camera.position.y;
        } else if (camera.position.y < lowerBound) {
            camera.position.y = lowerBound;
        } else if (camera.position.y > upperBound) {
            camera.position.y = upperBound;
        }

        clampCameraFocusToBounds(leftBound, lowerBound, rightBound, upperBound);
    }

    public void clampCameraFocusToBounds(float leftBound, float lowerBound, float rightBound, float upperBound) {
        this.cameraFocus.x = MathUtils.clamp(this.cameraFocus.x, leftBound, rightBound);
        this.cameraFocus.y = MathUtils.clamp(this.cameraFocus.y, lowerBound, upperBound);
    }

    private Vector3 unproject = new Vector3();

    public Vector2 toInGameCoordinates(int screenX, int screenY) {
        unproject.x = screenX;
        unproject.y = screenY;
        camera.unproject(this.unproject);
        return new Vector2(unproject.x, unproject.y);
    }

    public void setScreenSize(int screenWidth, int screenHeight) {
        camera.viewportWidth = screenWidth;
        camera.viewportHeight = screenHeight;
    }

    public void moveCameraFocus(Vector2 delta) {
        this.cameraFocus.add(delta);
    }

    public void setCameraFocus(Vector2 cameraFocus) {
        this.cameraFocus = cameraFocus;
    }

    public Vector2 getCameraFocus() {
        return cameraFocus;
    }

    public void setBounds(Vector2 bounds) {
        this.bounds.set(bounds);
    }

    public void setLowerBounds(Vector2 bounds) {
        this.lowerbounds.set(bounds);
    }

    public void setBoundsEnabled(boolean boundsEnabled) {
        this.boundsEnabled = boundsEnabled;
    }

    public void setSmoothing(float smoothing) {
        this.smoothing = smoothing;
    }

    public ScreenShake getScreenShake() {
        return screenShake;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setKeepAtPixels(boolean keepAtPixels) {
        this.keepAtPixels = keepAtPixels;
    }
}
