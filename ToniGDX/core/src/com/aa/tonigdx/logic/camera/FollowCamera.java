package com.aa.tonigdx.logic.camera;

import com.aa.tonigdx.logic.Entity;
import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Created by toni on 6/21/16.
 */
public class FollowCamera extends CameraManager{

    private Entity follow;
    private boolean followEnabled;

    public static FollowCamera createDefaultFollowCamera(int viewportWidth, int viewportHeight) {
        ScreenShake screenShake = new ScreenShake(1, 0.99f);
        OrthographicCamera camera = new OrthographicCamera(viewportWidth, viewportHeight);
        return new FollowCamera(camera, screenShake, 0.5f);
    }

    public FollowCamera(OrthographicCamera camera, ScreenShake screenShake, float smoothing) {
        super(camera, screenShake, smoothing);

        follow = null;
        followEnabled = true;
    }

    public void update() {
        if(followEnabled && follow != null){
            cameraFocus = follow.getCenter();
        }
        super.update();
    }

    public void setFollow(Entity follow) {
        this.follow = follow;
    }

    public void setFollowEnabled(boolean followEnabled) {
        this.followEnabled = followEnabled;
    }
}
