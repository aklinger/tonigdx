package com.aa.tonigdx.logic.camera;

/**
 * Created by Toni on 06.12.2014.
 */
public class ScreenShake {
    private float screenShake;
    private float shakeReduction = 1f;
    private float shakeDampening = 0.99f;
    private float screenDisplacementX = 0;
    private float screenDisplacementY = 0;

    public ScreenShake(float shakeReduction, float shakeDampening) {
        this.shakeReduction = shakeReduction;
        this.shakeDampening = shakeDampening;
    }

    public void update(){
        screenShake -= shakeReduction;
        screenShake *= shakeDampening;
        if (screenShake < 0) {
            screenShake = 0;
            screenDisplacementX = 0;
            screenDisplacementY = 0;
        } else {
            screenDisplacementX = (float) (Math.random() * screenShake) - screenShake / 2;
            screenDisplacementY = (float) (Math.random() * screenShake) - screenShake / 2;
        }
    }

    public void shakeScreen(float power){
        screenShake += power;
    }

    public void maintainScreenShakeLevel(float powerLevel){
        if(screenShake < powerLevel){
            screenShake = powerLevel;
        }
    }

    public float getScreenDisplacementX() {
        return screenDisplacementX;
    }

    public float getScreenDisplacementY() {
        return screenDisplacementY;
    }
}
