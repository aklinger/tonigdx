package com.aa.tonigdx.logic;

import com.aa.tonigdx.logic.camera.CameraManager;
import com.aa.tonigdx.logic.physics.SimpleCollisionResolver;
import com.badlogic.gdx.utils.Array;

/**
 * Created by toni on 6/26/16.
 */
public class AlternateEntityManager extends EntityManager {
    public AlternateEntityManager(CameraManager cameraManager) {
        super(cameraManager);
        setCollisionResolver(new SimpleCollisionResolver());
    }

    public AlternateEntityManager(int gameAreaWidth, int gameAreaHeight, CameraManager cameraManager) {
        super(gameAreaWidth, gameAreaHeight, cameraManager);
        setCollisionResolver(new SimpleCollisionResolver());
    }

    @Override
    protected void actEntities() {
        Array<Entity> entities = getEntities();
        for (Entity ent1 : entities) {
            ent1.act(this);
            if (!ent1.isAlive()) {
                killEntity(ent1, true);
            }
        }
        for (Entity ent1 : entities) {
            ent1.afterAct(this);
        }
    }
}
