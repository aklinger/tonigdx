package com.aa.tonigdx.logic;

import com.aa.tonigdx.logic.animation.AnimationSheet;
import com.aa.tonigdx.maths.Collision;
import com.aa.tonigdx.maths.HitBox;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 15.04.2014.
 */
public abstract class Entity implements HitBox {

    private float x;
    private float y;
    private float sx;
    private float sy;
    private float gx;
    private float gy;
    private float fx;
    private float fy;
    private float lastX;
    private float lastY;
    private float lastX2;
    private float lastY2;

    private AnimationSheet animationSheet;
    private float scale = 1;
    private Color color;
    private float imgW;
    private float imgH;
    private float rotation;
    private boolean flipX;
    private boolean flipY;
    private float originX;
    private float originY;
    private float imgX;
    private float imgY;
    private int zDepth;
    private final Vector2 centerOffset = new Vector2();

    private Rectangle hitBox;
    private int collisionLayers;
    private int presentLayers;
    private boolean hitBoxDirty = true;
    public static final int LAYER_ENTITY = 0x1;
    public static final int LAYER_TILEMAP = 0x2;
    private boolean fixated;
    private float mass;
    private float bounciness;
    public int pushProtection;

    private float maxHealth;
    private float health;

    public Entity() {
        fx = 1;
        fy = 1;
        collisionLayers = LAYER_ENTITY | LAYER_TILEMAP;
        presentLayers = LAYER_ENTITY;
        mass = 1;
        bounciness = 0.5f;
        maxHealth = health = 10;
        pushProtection = 1;
        color = Color.WHITE;
    }

    public void executeMovement() {
        executeMovement(true);
    }

    public void executeMovement(boolean gravity) {
        computeMovement(gravity);
        x += sx;
        y += sy;
    }

    public void computeMovement(boolean gravity) {
        if (gravity) {
            sx += gx;
            sy += gy;
        }
        sx *= fx;
        sy *= fy;
        if (fixated) {
            sx = 0;
            sy = 0;
        }
    }

    public abstract void act(EntityManager man);

    public boolean willResolveCollisionWith(Entity other) {
        return this.isSolidEntity() && other.isSolidEntity();
    }

    public static boolean willResolveCollision(Entity first, Entity second) {
        return first.willResolveCollisionWith(second) && second.willResolveCollisionWith(first);
    }

    public static boolean canCollideWith(Entity first, Entity second) {
        return (first.presentLayers & second.collisionLayers) != 0 || (second.presentLayers & first.collisionLayers) != 0;
    }

    public abstract void interactWith(Entity ent2, Collision col, EntityManager man);

    public void afterAct(EntityManager man) {
        if (pushProtection > 0) {
            pushProtection--;
        }
        lastX2 = lastX;
        lastX = x;
        lastY2 = lastY;
        lastY = y;
    }

    public void render(Batch batch) {
        batch.setColor(color);
        TextureRegion currentFrame = animationSheet.getCurrentFrame();
        batch.draw(currentFrame.getTexture(), getX() + imgX, getY() + imgY, originX, originY, imgW, imgH, scale, scale, rotation, currentFrame.getRegionX(), currentFrame.getRegionY(), currentFrame.getRegionWidth(), currentFrame.getRegionHeight(), flipX, flipY);
        batch.setColor(Color.WHITE);
    }

    public void debugRender(ShapeRenderer renderer) {
        renderer.begin(ShapeRenderer.ShapeType.Line);
        Rectangle hitBox1 = getBoundingBox();
        renderer.rect(hitBox1.x, hitBox1.y, hitBox1.width, hitBox1.height);
        renderer.end();
    }

    public void onSpawn(EntityManager manager) {
        //NOOP
    }

    public abstract void die(EntityManager man);

    public abstract void cleanup(EntityManager man);

    public void setMaxHealthFull(float maxHealth) {
        setMaxHealth(maxHealth, false);
        setHealth(maxHealth);
    }

    public void setMaxHealth(float maxHealth, boolean keepHealthRelative) {
        float ratio;
        if (this.maxHealth != 0) {
            ratio = health / this.maxHealth;
        } else {
            ratio = 1;
        }
        this.maxHealth = maxHealth;
        if (keepHealthRelative) {
            health = this.maxHealth * ratio;
        } else if (health > maxHealth) {
            health = maxHealth;
        }
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public void damage(Entity damageDealer, float damage) {
        health -= damage;
    }

    public void heal(float heal) {
        health += heal;
        if (health > maxHealth) {
            health = maxHealth;
        }
    }

    public boolean isAlive() {
        return health > 0;
    }

    public void rotateToDirectionMoving(float offset) {
        float rot = (float) Math.toDegrees(Math.atan2(-getSx(), getSy()));
        rotation = rot + offset;
    }

    public void addPosition(float deltax, float deltay) {
        x += deltax;
        y += deltay;
    }

    public void addPosition(Vector2 delta) {
        x += delta.x;
        y += delta.y;
    }

    public Vector2 getSpeed() {
        return new Vector2(sx, sy);
    }

    public void setSpeed(Vector2 speed) {
        sx = speed.x;
        sy = speed.y;
    }

    public void addSpeed(Vector2 delta) {
        addSpeed(delta.x, delta.y);
    }

    public void addSpeed(float dx, float dy) {
        this.sx += dx;
        this.sy += dy;
    }

    public void rotate(float deltaRotation) {
        rotation += deltaRotation;
    }

    @Override
    public Entity getParent() {
        return this;
    }

    @Override
    public int getHitboxType() {
        return HitBox.AABB;
    }

    private Rectangle boundingBox = new Rectangle();

    public Rectangle getBoundingBox() {
        if (hitBox != null) {
            return boundingBox.set(x + hitBox.x, y + hitBox.y, hitBox.getWidth(), hitBox.getHeight());
        } else {
            return boundingBox.set(x + imgX, y + imgY, imgW, imgH);
        }
    }

    public float getBBWidth() {
        if (hitBox != null) {
            return hitBox.width;
        } else {
            return imgW;
        }
    }

    public float getBBHeight() {
        if (hitBox != null) {
            return hitBox.height;
        } else {
            return imgH;
        }
    }

    public float getBBBottom() {
        if (hitBox != null) {
            return y + hitBox.y;
        } else {
            return y;
        }
    }

    public float getBBLeft() {
        if (hitBox != null) {
            return x + hitBox.x;
        } else {
            return x;
        }
    }

    public Vector2 getCenter() {
        if (hitBoxDirty) {
            updateCenterOffset();
        }
        return new Vector2(x + centerOffset.x, y + centerOffset.y);
    }

    public Vector2 getCenterReuse(Vector2 output) {
        if (hitBoxDirty) {
            updateCenterOffset();
        }
        return output.set(x + centerOffset.x, y + centerOffset.y);
    }

    private void updateCenterOffset() {
        if (hitBox != null) {
            centerOffset.set(hitBox.x + hitBox.width / 2, hitBox.y + hitBox.height / 2);
        } else {
            centerOffset.set(imgX + imgW / 2, imgY + imgH / 2);
        }
        hitBoxDirty = false;
    }

    public void setCenter(float x, float y) {
        this.x = x - (hitBox.x + hitBox.getWidth() / 2);
        this.y = y - (hitBox.y + hitBox.getHeight() / 2);
    }

    public void setCenter(Vector2 cent) {
        setCenter(cent.x, cent.y);
    }

    @Override
    public float getRadius() {
        if (hitBox != null) {
            return hitBox.getWidth() / 2;
        } else {
            return imgW / 2;
        }
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getHitBoxXOffset() {
        return this.hitBox.x;
    }

    public float getHitBoxYOffset() {
        return this.hitBox.y;
    }

    public float getLastX() {
        return lastX;
    }

    public float getLastY() {
        return lastY;
    }

    public float getXMovementLastFrame() {
        return lastX - lastX2;
    }

    public float getYMovementLastFrame() {
        return lastY - lastY2;
    }

    public Vector2 getMovementLastFrame() {
        return new Vector2(lastX - lastX2, lastY - lastY2);
    }

    public float getSx() {
        return sx;
    }

    public void setSx(float sx) {
        this.sx = sx;
    }

    public float getSy() {
        return sy;
    }

    public void setSy(float sy) {
        this.sy = sy;
    }

    public float getGx() {
        return gx;
    }

    public void setGx(float gx) {
        this.gx = gx;
    }

    public float getGy() {
        return gy;
    }

    public void setGy(float gy) {
        this.gy = gy;
    }

    public float getFx() {
        return fx;
    }

    public void setFx(float fx) {
        this.fx = fx;
    }

    public float getFy() {
        return fy;
    }

    public void setFy(float fy) {
        this.fy = fy;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public void clearPresentAndCollisionLayers() {
        presentLayers = 0;
        collisionLayers = 0;
    }

    public boolean isPresentOnLayer(int layerMask) {
        return (presentLayers & layerMask) != 0;
    }

    public boolean isCollidingWithLayer(int layerMask) {
        return (collisionLayers & layerMask) != 0;
    }

    public void setLayer(boolean presentOn, boolean collidesWith, int layerMask) {
        setPresentLayer(presentOn, layerMask);
        setCollisionLayer(collidesWith, layerMask);
    }

    public void setPresentLayer(boolean presentOn, int layerMask) {
        if (presentOn) {
            presentLayers |= layerMask;
        } else {
            presentLayers &= (~layerMask);
        }
    }

    public void setCollisionLayer(boolean collidesWith, int layerMask) {
        if (collidesWith) {
            collisionLayers |= layerMask;
        } else {
            collisionLayers &= (~layerMask);
        }
    }

    public boolean isSolidTilemap() {
        return isPresentOnLayer(LAYER_TILEMAP);
    }

    public void setSolidTilemap(boolean solidTilemap) {
        setLayer(solidTilemap, solidTilemap, LAYER_TILEMAP);
    }

    public boolean isSolidEntity() {
        return isPresentOnLayer(LAYER_ENTITY);
    }

    public void setSolidEntity(boolean solidEntity) {
        setLayer(solidEntity, solidEntity, LAYER_ENTITY);
    }

    public boolean isFixated() {
        return fixated;
    }

    public void setFixated(boolean fixated) {
        this.fixated = fixated;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public float getBounciness() {
        return bounciness;
    }

    public void setBounciness(float bounciness) {
        this.bounciness = bounciness;
    }

    public boolean isFlipX() {
        return flipX;
    }

    public void setFlipX(boolean flipX) {
        this.flipX = flipX;
    }

    public boolean isFlipY() {
        return flipY;
    }

    public void setFlipY(boolean flipY) {
        this.flipY = flipY;
    }

    public void setDrawableSize(float width, float height, boolean updateHitbox) {
        this.imgW = width;
        this.imgH = height;
        this.originX = imgW / 2;
        this.originY = imgH / 2;
        if (updateHitbox) {
            this.setHitBox(new Rectangle(imgX, imgY, imgW, imgH));
        }
    }

    public void setDrawableOffset(float x, float y) {
        this.imgX = x;
        this.imgY = y;
    }

    public float getDrawableOffsetX() {
        return imgX;
    }

    public float getDrawableOffsetY() {
        return imgY;
    }

    public float getDrawableX() {
        return x + imgX;
    }

    public float getDrawableY() {
        return y + imgY;
    }

    public void rescale(float scaleX, float scaleY) {
        Vector2 center = this.getCenter();
        this.imgW = imgW * scaleX;
        this.imgH = imgH * scaleY;
        this.originX = imgW / 2;
        this.originY = imgH / 2;
        this.setHitBox(new Rectangle(hitBox.x * scaleX, hitBox.y * scaleY, hitBox.width * scaleX, hitBox.height * scaleY));
        this.setCenter(center);
    }

    public void placeOnPlanet(Entity planet) {
        Vector2 delta = getCenter().sub(planet.getCenter());
        this.setRotation(delta.angle() - 90);
        this.setCenter(planet.getCenter().add(delta.nor().scl(planet.getRadius() + getRadius())));
    }

    public void setAnimationSheet(AnimationSheet animationSheet, boolean updateDrawableSize, boolean updateHitBox) {
        this.animationSheet = animationSheet;
        if (updateDrawableSize && animationSheet != null) {
            setDrawableSize(animationSheet.getCurrentFrame().getRegionWidth(), animationSheet.getCurrentFrame().getRegionHeight(), updateHitBox);
        }
    }

    public void setAnimationSheet(AnimationSheet animationSheet, int drawableWidth, int drawableHeight, int hitBoxUpPadding, int hitBoxDownPadding, int hitBoxLeftPadding, int hitBoxRightPadding) {
        this.animationSheet = animationSheet;
        this.imgW = drawableWidth;
        this.imgH = drawableHeight;
        this.originX = imgW / 2;
        this.originY = imgH / 2;
        setHitBox(hitBoxUpPadding, hitBoxDownPadding, hitBoxLeftPadding, hitBoxRightPadding);
    }

    public void setAnimationSheetCentral(AnimationSheet animationSheet, int drawableWidth, int drawableHeight, int hitBoxUpPadding, int hitBoxDownPadding, int hitBoxLeftPadding, int hitBoxRightPadding) {
        this.animationSheet = animationSheet;
        this.imgX = -drawableWidth / 2 + (hitBoxRightPadding - hitBoxLeftPadding) / 2;
        this.imgY = -drawableHeight / 2 + (hitBoxUpPadding - hitBoxDownPadding) / 2;
        this.imgW = drawableWidth;
        this.imgH = drawableHeight;
        this.originX = drawableWidth / 2 - (hitBoxRightPadding - hitBoxLeftPadding) / 2;
        this.originY = drawableHeight / 2 - (hitBoxUpPadding - hitBoxDownPadding) / 2;
        setHitBoxCentral(drawableWidth - (hitBoxLeftPadding + hitBoxRightPadding), drawableHeight - (hitBoxUpPadding + hitBoxDownPadding));
    }

    public void setHitBoxCentral(float width, float height) {
        this.hitBox = new Rectangle(-width / 2, -height / 2, width, height);
        this.centerOffset.setZero();
    }

    public void setHitBox(int hitBoxUpPadding, int hitBoxDownPadding, int hitBoxLeftPadding, int hitBoxRightPadding) {
        this.hitBox = new Rectangle(hitBoxLeftPadding, hitBoxDownPadding, imgW - (hitBoxLeftPadding + hitBoxRightPadding), imgH - (hitBoxUpPadding + hitBoxDownPadding));
        this.hitBoxDirty = true;
    }

    public AnimationSheet getAnimationSheet() {
        return animationSheet;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public float getHealth() {
        return health;
    }

    public void setPushProtection(int pushProtection) {
        this.pushProtection = pushProtection;
    }

    public void setHitBox(Rectangle hitBox) {
        this.hitBox = hitBox;
        this.hitBoxDirty = true;
    }

    public boolean isForeground() {
        return false;
    }

    public boolean isPushProtection() {
        return pushProtection > 0;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public float getImgW() {
        return imgW;
    }

    public float getImgH() {
        return imgH;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void hasHitTileMap(EntityManager manager, Collision collision) {
    }

    public void squish(EntityManager manager, Collision collision, Entity pushedBy) {
        manager.killEntity(this, true);
    }

    public boolean isRiding(Entity entity) {
        return false;
    }

    public boolean canPush(Entity entity) {
        return this.mass >= entity.mass;
    }

    public void recenterImageOnHitbox() {
        float centerx = hitBox.x + hitBox.width / 2;
        float centery = hitBox.y + hitBox.height / 2;
        imgX = centerx - imgW / 2;
        imgY = centery - imgH / 2;
        originX = imgW / 2;
        originY = imgH / 2;
    }

    public void setImgOrigin(float originX, float originY) {
        this.originX = originX;
        this.originY = originY;
    }

    public void placeOriginAt(Vector2 position) {
        placeOriginAt(position.x, position.y);
    }

    public void placeOriginAt(float positionX, float positionY) {
        x = positionX - this.originX;
        y = positionY - this.originX;
    }

    public void setZDepth(int zDepth) {
        this.zDepth = zDepth;
    }

    public int getZDepth() {
        return zDepth;
    }
}
