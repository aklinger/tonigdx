package com.aa.tonigdx.logic.tween;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by Toni on 18.07.2015.
 */
public class LabelAccessor implements TweenAccessor<Label> {

    public static final int FONT_SCALE = 101;

    @Override
    public int getValues(Label target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case FONT_SCALE:
                returnValues[0] = target.getFontScaleX();
                return 1;
            default:
                return -1;
        }

    }

    @Override
    public void setValues(Label target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case FONT_SCALE:
                target.setFontScale(newValues[0]);
                break;
            default:
                throw new IllegalArgumentException("Invalid tweentype ("+tweenType+") for LabelAccessor.");
        }
    }
}
