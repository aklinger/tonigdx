package com.aa.tonigdx.logic.tween;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Toni on 04.12.2014.
 */
public class ActorAccessor implements TweenAccessor<Actor> {

    public static final int POSITION_X = 1;
    public static final int POSITION_Y = 2;
    public static final int POSITION_XY = 3;
    public static final int ROTATION = 4;
    public static final int COLOR = 5;
    public static final int SCALE_UNIFORM = 6;
    public static final int POSITION_CENTER_X = 7;
    public static final int POSITION_CENTER_Y = 8;
    public static final int POSITION_CENTER_XY = 9;

    @Override
    public int getValues(Actor target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X: returnValues[0] = target.getX(); return 1;
            case POSITION_Y: returnValues[0] = target.getY(); return 1;
            case POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2;
            case POSITION_CENTER_X: returnValues[0] = target.getX(Align.center); return 1;
            case POSITION_CENTER_Y: returnValues[0] = target.getY(Align.center); return 1;
            case POSITION_CENTER_XY:
                returnValues[0] = target.getX(Align.center);
                returnValues[1] = target.getY(Align.center);
                return 2;
            case ROTATION:
                returnValues[0] = target.getRotation();
                return 1;
            case COLOR:
                returnValues[0] = target.getColor().r;
                returnValues[1] = target.getColor().g;
                returnValues[2] = target.getColor().b;
                returnValues[3] = target.getColor().a;
                return 4;
            case SCALE_UNIFORM:
                returnValues[0] = target.getScaleX();
                return 1;
            default:
                return -1;
        }

    }

    @Override
    public void setValues(Actor target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POSITION_X: target.setX(newValues[0]); break;
            case POSITION_Y: target.setY(newValues[0]); break;
            case POSITION_XY:
                target.setX(newValues[0]);
                target.setY(newValues[1]);
                break;
            case POSITION_CENTER_X: target.setX(newValues[0], Align.center); break;
            case POSITION_CENTER_Y: target.setY(newValues[0], Align.center); break;
            case POSITION_CENTER_XY:
                target.setX(newValues[0], Align.center);
                target.setY(newValues[1], Align.center);
                break;
            case ROTATION:
                target.setRotation(newValues[0]);
                break;
            case COLOR:
                target.setColor(newValues[0],newValues[1],newValues[2],newValues[3]);
                break;
            case SCALE_UNIFORM:
                target.setScale(newValues[0]);
                break;
            default:
                throw new IllegalArgumentException("Invalid tweentype ("+tweenType+") for ActorAccessor.");
        }
    }
}
