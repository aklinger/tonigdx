package com.aa.tonigdx.logic.tween;

import aurelienribon.tweenengine.TweenAccessor;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Created by toni on 8/21/16.
 */
public class SpriteAccessor implements TweenAccessor<Sprite> {

    public static final int POSITION_X = 0;
    public static final int POSITION_Y = 1;
    public static final int POSITION_XY = 2;
    public static final int COLOR_RGB = 3;
    public static final int COLOR_RGBA = 4;
    public static final int ROTATION = 5;
    public static final int SCALE_XY = 6;
    public static final int SCALE_X = 7;
    public static final int SCALE_Y = 8;

    @Override
    public int getValues(Sprite target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POSITION_X:
                returnValues[0] = target.getX();
                return 1;
            case POSITION_Y:
                returnValues[0] = target.getY();
                return 1;
            case POSITION_XY:
                returnValues[0] = target.getX();
                returnValues[1] = target.getY();
                return 2;
            case ROTATION:
                returnValues[0] = target.getRotation();
                return 1;
            case COLOR_RGBA:
                returnValues[0] = target.getColor().r;
                returnValues[1] = target.getColor().g;
                returnValues[2] = target.getColor().b;
                returnValues[3] = target.getColor().a;
                return 4;
            case COLOR_RGB:
                returnValues[0] = target.getColor().r;
                returnValues[1] = target.getColor().g;
                returnValues[2] = target.getColor().b;
                return 3;
            case SCALE_XY:
                returnValues[0] = target.getScaleX();
                returnValues[1] = target.getScaleY();
                return 2;
            case SCALE_X:
                returnValues[0] = target.getScaleX();
                return 1;
            case SCALE_Y:
                returnValues[0] = target.getScaleY();
                return 1;
            default:
                return -1;
        }
    }

    @Override
    public void setValues(Sprite target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POSITION_X:
                target.setX(newValues[0]);
                break;
            case POSITION_Y:
                target.setY(newValues[0]);
                break;
            case POSITION_XY:
                target.setX(newValues[0]);
                target.setY(newValues[1]);
                break;
            case ROTATION:
                target.setRotation(newValues[0]);
                break;
            case COLOR_RGBA:
                target.setColor(newValues[0], newValues[1], newValues[2], newValues[3]);
                break;
            case COLOR_RGB:
                target.setColor(newValues[0], newValues[1], newValues[2], target.getColor().a);
                break;
            case SCALE_XY:
                target.setScale(newValues[0], newValues[1]);
                break;
            case SCALE_X:
                target.setScale(newValues[0], target.getScaleY());
                break;
            case SCALE_Y:
                target.setScale(target.getScaleX(), newValues[0]);
                break;
            default:
                throw new IllegalArgumentException("Invalid tweentype (" + tweenType + ") for SpriteAccessor.");
        }
    }
}
