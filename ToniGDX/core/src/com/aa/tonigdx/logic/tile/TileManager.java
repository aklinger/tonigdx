package com.aa.tonigdx.logic.tile;

import com.aa.tonigdx.logic.Entity;
import com.aa.tonigdx.logic.EntityManager;
import com.aa.tonigdx.logic.physics.CollisionResolver;
import com.aa.tonigdx.maths.Collision;
import com.aa.tonigdx.maths.CollisionUtil;
import com.aa.tonigdx.maths.HitBox;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Toni on 26.04.2014.
 */
public class TileManager {

    private TileMap map;

    public TileManager(TileMap tileMap) {
        map = tileMap;
    }

    public void fillMapWith(Tile tileType) {
        for (int i = 0; i < map.getMapWidth(); i++) {
            for (int j = 0; j < map.getMapHeight(); j++) {
                map.setTile(i, j, tileType);
            }
        }
    }

    public void createMapBorder(int borderSizeX, int borderSizeY, Tile tileType) {
        for (int i = 0; i < map.getMapWidth(); i++) {
            for (int j = 0; j < map.getMapHeight(); j++) {
                if (i < borderSizeX || i > map.getMapWidth() - borderSizeX - 1 || j < borderSizeY || j > map.getMapHeight() - borderSizeY - 1) {
                    map.setTile(i, j, tileType);
                }
            }
        }
    }

    private static final Vector2 temp1 = new Vector2();
    private static final Vector2 temp2 = new Vector2();
    private static final Vector2 temp3 = new Vector2();

    //Render Method
    public void render(Batch batch, Vector3 translation, int gameWidth, int gameHeight) {
        Vector2 pos1 = getIndexAtReuse(translation.x - gameWidth, translation.y - gameHeight, temp1);
        Vector2 pos2 = getIndexAtReuse(translation.x + gameWidth, translation.y + gameHeight, temp2);
        for (int i = (int) pos1.x; i <= (int) (pos2.x); i++) {
            for (int j = (int) pos1.y; j <= (int) pos2.y; j++) {
                Tile t = map.getTile(i, j);
                t.render(batch, this, temp3.set(i, j));
            }
        }
    }

    //GamePlay Methods
    private Entity dummy = new Dummy();
    private Rectangle tempRectangle = new Rectangle();

    public boolean checkCollision(Entity entity, EntityManager man, CollisionResolver collisionResolver, Entity pushedBy) {
        boolean hit = false;
        Rectangle box = entity.getBoundingBox();
        Vector2 entPos1 = getIndexAtReuse(box.getX(), box.getY(), temp1);
        Vector2 entPos2 = getIndexAtReuse(box.getX() + box.getWidth(), box.getY() + box.getHeight(), temp2);
        dummy.setFixated(true);
        dummy.setSolidEntity(true);
        for (int i = (int) entPos1.x; i <= (int) (entPos2.x); i++) {
            for (int j = (int) entPos1.y; j <= (int) entPos2.y; j++) {
                if (isCollidableWithEntity(entity, i, j)) {
                    if (j + 1 <= entPos2.y && isCollidableWithEntity(entity, i, j + 1)) {
                        tempRectangle.set(i * map.getTileSize(), j * map.getTileSize(), map.getTileSize(), map.getTileSize() * 2);
                    } else if (i + 1 <= entPos2.x && isCollidableWithEntity(entity, i + 1, j)) {
                        tempRectangle.set(i * map.getTileSize(), j * map.getTileSize(), map.getTileSize() * 2, map.getTileSize());
                    } else {
                        tempRectangle.set(i * map.getTileSize(), j * map.getTileSize(), map.getTileSize(), map.getTileSize());
                    }
                    dummy.setHitBox(tempRectangle);
                    Collision col = CollisionUtil.calcCollisionHitBoxes(entity, dummy, true, false);
                    if (col != null) {
                        entity.hasHitTileMap(man, col);
                        col.setMain(entity);
                        if (entity.isCollidingWithLayer(Entity.LAYER_TILEMAP)) {
                            collisionResolver.resolveCollision(col, !entity.isPushProtection());
                            if (pushedBy != null) {
                                entity.squish(man, col, pushedBy);
                            }
                        }
                        hit = true;
                    }
                }
            }
        }
        return hit;
    }

    private boolean isCollidableWithEntity(Entity entity, int xIndex, int yIndex) {
        boolean solid = map.getTileProperty(xIndex, yIndex, TileMap.PROPERTY_SOLID);
        if (solid) {
            return true;
        }
        float cloudGrazeDist = 4;
        boolean cloud = map.getTileProperty(xIndex, yIndex, TileMap.PROPERTY_CLOUD);
        if (cloud && entity.getLastY() + entity.getHitBoxYOffset() + cloudGrazeDist >= (yIndex + 1) * map.getTileSize()) {
            return true;
        }
        return false;
    }

    private class Dummy extends Entity {
        @Override
        public void act(EntityManager man) {

        }

        @Override
        public void interactWith(Entity ent2, Collision col, EntityManager man) {

        }

        @Override
        public void die(EntityManager man) {

        }

        @Override
        public void cleanup(EntityManager man) {

        }

        @Override
        public boolean willResolveCollisionWith(Entity other) {
            return other.isSolidTilemap();
        }

        @Override
        public int getHitboxType() {
            return HitBox.AABB;
        }
    }

    //Collision Methods
    public boolean isTouchingGround(Entity obj, String property) {
        if (obj.isSolidTilemap()) {
            Rectangle rect = obj.getBoundingBox();
            int xdiff = (int) (rect.getWidth() / 2) - 1;
            int yreach = -1;
            rect.getCenter(center);
            Vector2 bottom;
            bottom = new Vector2(center.x + xdiff, rect.getY() + yreach);
            if (getTilePropertyOfTileAt(bottom, property)) {
                return true;
            }
            bottom = new Vector2(center.x - xdiff, rect.getY() + yreach);
            if (getTilePropertyOfTileAt(bottom, property)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingGround(Entity obj) {
        return isTouchingGround(obj, TileMap.PROPERTY_SOLID);
    }

    public boolean isTouchingGroundLeft(Entity obj, String property) {
        if (obj.isSolidTilemap()) {
            Rectangle rect = obj.getBoundingBox();
            int xdiff = (int) ((rect.getWidth()) / 2) - 1;
            int yreach = -1;
            rect.getCenter(center);
            Vector2 bottom = new Vector2(center.x - xdiff, rect.getY() + yreach);
            if (getTilePropertyOfTileAt(bottom, property)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingGroundLeft(Entity obj) {
        return isTouchingGroundLeft(obj, TileMap.PROPERTY_SOLID);
    }

    public boolean isTouchingGroundRight(Entity obj, String property) {
        if (obj.isSolidTilemap()) {
            Rectangle rect = obj.getBoundingBox();
            int xdiff = (int) (rect.getWidth() / 2) - 1;
            int yreach = -1;
            rect.getCenter(center);
            Vector2 bottom = new Vector2(center.x + xdiff, rect.getY() + yreach);
            if (getTilePropertyOfTileAt(bottom, property)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingGroundRight(Entity obj) {
        return isTouchingGroundRight(obj, TileMap.PROPERTY_SOLID);
    }

    public boolean isTouchingWall(Entity obj, boolean left, String property) {
        if (obj.isSolidTilemap()) {
            Rectangle rect = obj.getBoundingBox();
            int ydiff = (int) (rect.getHeight() / 2) - 1;
            int xreach = left ? -1 : (int) (rect.getWidth() + 1);
            rect.getCenter(center);
            Vector2 bottom = new Vector2(rect.getX() + xreach, center.y - ydiff);
            if (getTilePropertyOfTileAt(bottom, property)) {
                return true;
            }
            Vector2 top = new Vector2(rect.getX() + xreach, center.y + ydiff);
            if (getTilePropertyOfTileAt(top, property)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingWall(Entity obj, boolean left) {
        return isTouchingWall(obj, left, TileMap.PROPERTY_SOLID);
    }

    private volatile Vector2 center = new Vector2();
    private volatile Vector2 side = new Vector2();

    public Vector2 getPositionOfCorner(Entity obj, Vector2 direction, boolean left, float scanReach) {
        direction.nor();
        side.set(direction).rotate90(left ? -1 : 1);
        Rectangle rect = obj.getBoundingBox();
        float xdiff = (rect.getWidth() / 2 * direction.x) + direction.x * scanReach + (rect.getWidth() / 2 - 1) * side.x;
        float ydiff = (rect.getHeight() / 2 * direction.y) + direction.y * scanReach + (rect.getHeight() / 2 - 1) * side.y;
        rect.getCenter(center);
        return center.cpy().add(xdiff, ydiff);
    }

    public boolean isTouchingSolidTile(Entity obj, Vector2 direction, boolean left, float scanReach) {
        if (obj.isSolidTilemap()) {
            Vector2 tilePosition = getPositionOfCorner(obj, direction, left, scanReach);
            if (getTilePropertyOfTileAt(tilePosition, TileMap.PROPERTY_SOLID)) {
                return true;
            }
        }
        return false;
    }

    public boolean isTouchingTileWithProperty(Entity obj, Vector2 direction, boolean left, float scanReach, String property) {
        if (obj.isSolidTilemap()) {
            Vector2 tilePosition = getPositionOfCorner(obj, direction, left, scanReach);
            if (getTilePropertyOfTileAt(tilePosition, property)) {
                return true;
            }
        }
        return false;
    }


    //Management Methods
    public boolean getTilePropertyOfTileAt(Vector2 position, String property) {
        return map.getTileProperty((int) Math.floor(position.x / map.getTileSize()), (int) Math.floor(position.y / map.getTileSize()), property);
    }

    public Vector2 getPositionAtIndex(int x, int y) {
        return new Vector2(x * map.getTileSize() + map.getTileSize() / 2, y * map.getTileSize() + map.getTileSize() / 2);
    }

    public Tile getTileAtPosition(float x, float y) {
        return map.getTile((int) Math.floor(x / map.getTileSize()), (int) Math.floor(y / map.getTileSize()));
    }

    public Tile getTileAtPosition(Vector2 position) {
        return map.getTile((int) Math.floor(position.x / map.getTileSize()), (int) Math.floor(position.y / map.getTileSize()));
    }

    public Vector2 getIndexAt(float x, float y) {
        return new Vector2((int) Math.floor(x / map.getTileSize()), (int) Math.floor(y / map.getTileSize()));
    }

    public Vector2 getIndexAtReuse(float x, float y, Vector2 output) {
        return output.set((int) Math.floor(x / map.getTileSize()), (int) Math.floor(y / map.getTileSize()));
    }

    public Vector2 getIndexAt(Vector2 position) {
        return new Vector2((int) Math.floor(position.x / map.getTileSize()), (int) Math.floor(position.y / map.getTileSize()));
    }

    public int getTotalWidth() {
        return (int) (map.getMapWidth() * map.getTileSize());
    }

    public int getTotalHeight() {
        return (int) (map.getMapHeight() * map.getTileSize());
    }

    public TileMap getMap() {
        return map;
    }
}
