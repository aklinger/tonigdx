package com.aa.tonigdx.logic.tile;

/**
 * Created by Toni on 04.06.2015.
 */
public interface TileMap {

    String PROPERTY_SOLID = "solid";
    String PROPERTY_CLOUD = "cloud";

    boolean getTileProperty(int x, int y, String property);
    Tile getTile(int x, int y);
    void setTile(int x, int y, Tile tile);
    float getTileSize();
    int getMapWidth();
    int getMapHeight();
}
