package com.aa.tonigdx.logic.tile;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 26.04.2014.
 */
public abstract class Tile {

    private TextureRegion texture;

    public boolean solid;

    public Tile(TextureRegion texture, boolean solid) {
        this.texture = texture;
        this.solid = solid;
    }

    protected abstract Tile baseTile();

    public void render(Batch batch, TileManager tileManager, Vector2 position) {
        if (texture != null) {
            float tileSize = tileManager.getMap().getTileSize();
            batch.draw(texture, (int) (position.x * tileSize), (int) (position.y * tileSize), tileSize, tileSize);
        }
    }

    public TextureRegion getTexture() {
        return texture;
    }
}
