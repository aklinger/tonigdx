package com.aa.tonigdx.logic.tile;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

/**
 * Created by Toni on 04.06.2015.
 */
public class TiledCustomMap implements TileMap{

    private TiledMapTileLayer tiledMapTileLayer;
    private float unitScale;

    public TiledCustomMap(TiledMapTileLayer tiledMapTileLayer, float unitScale) {
        this.tiledMapTileLayer = tiledMapTileLayer;
        this.unitScale = unitScale;
    }

    @Override
    public boolean getTileProperty(int x, int y, String property) {
        if(x < 0){
            x = 0;
        }else if(x >= tiledMapTileLayer.getWidth()){
            x = tiledMapTileLayer.getWidth()-1;
        }
        if(y < 0){
            y = 0;
        }else if(y >= tiledMapTileLayer.getHeight()){
            y = tiledMapTileLayer.getHeight()-1;
        }
        TiledMapTileLayer.Cell cell =  tiledMapTileLayer.getCell(x, y);
        if(cell != null) {
            return Boolean.parseBoolean(cell.getTile().getProperties().get(property, String.class));
        }else{
            return false;
        }
    }

    @Override
    public Tile getTile(int x, int y) {
        throw new RuntimeException("operation not supported");
    }

    @Override
    public void setTile(int x, int y, Tile tile) {
        throw new RuntimeException("operation not supported");
    }

    @Override
    public float getTileSize() {
        return tiledMapTileLayer.getTileWidth()*unitScale;
    }

    @Override
    public int getMapWidth() {
        return tiledMapTileLayer.getWidth();
    }

    @Override
    public int getMapHeight() {
        return tiledMapTileLayer.getHeight();
    }

    public TiledMapTileLayer getTiledMapTileLayer() {
        return tiledMapTileLayer;
    }
}
