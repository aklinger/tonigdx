package com.aa.tonigdx.logic.tile;

/**
 * Created by Toni on 04.06.2015.
 */
public class ArrayMap implements TileMap {

    private Tile[][] map;
    private float tileSize;
    private Tile borderTile;

    public ArrayMap(int width, int height, float tileSize, Tile borderTile){
        map = new Tile[width][height];
        this.tileSize = tileSize;
        this.borderTile = borderTile;
    }

    @Override
    public boolean getTileProperty(int x, int y, String property) {
        return getTile(x,y).solid;//FIXME: WHAT THE HELL IS THIS
    }

    @Override
    public Tile getTile(int x, int y){
        if(rangeCheck(x,y)){
            return map[x][y];
        }else{
            return borderTile;
        }
    }

    @Override
    public void setTile(int x, int y, Tile tile){
        if(rangeCheck(x,y)) {
            map[x][y] = tile;
        }
    }

    public boolean rangeCheck(int x, int y){
        return (x >= 0 && y >= 0 && x < getMapWidth() && y < getMapHeight());
    }

    @Override
    public int getMapWidth(){
        return map.length;
    }
    @Override
    public int getMapHeight(){
        return map[0].length;
    }

    @Override
    public float getTileSize() {
        return tileSize;
    }
}
