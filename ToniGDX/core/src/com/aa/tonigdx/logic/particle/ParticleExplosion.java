package com.aa.tonigdx.logic.particle;

import com.aa.tonigdx.dal.ResourceProvider;
import com.aa.tonigdx.maths.Calc;
import com.badlogic.gdx.graphics.Color;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A Builder for particle explosions.
 * <p/>
 *
 * @author Toni
 */
public class ParticleExplosion {
    private float x;
    private float y;
    private float rotationSpeed;
    private float amount = 1;
    private float displacementX;
    private float displacementY;
    private float angleRange = (float) Calc.TWO_PI;
    private float minSpeed;
    private float maxSpeed;
    private float frictionX = 1;
    private float frictionY = 1;
    private float gravityX;
    private float gravityY;
    private String picture;
    private int minDuration;
    private int maxDuration;
    private float size = 16;
    private boolean fadeOut;
    private Color color;

    public Collection<Particle> particleExplosion(ResourceProvider rp) {
        Collection<Particle> parts = new ArrayList<>();
        SimpleParticle p;
        for (int i = 0; i < amount; i++) {
            p = getInstanceOfNewParticle();

            initParticle(p, rp);

            parts.add(p);
        }
        return parts;
    }

    protected SimpleParticle getInstanceOfNewParticle() {
        return new SimpleParticle();
    }

    protected void initParticle(SimpleParticle p, ResourceProvider resourceProvider) {
        double spawnDirection = Math.random() * Calc.TWO_PI;
        float posx = x + (float) (Math.sin(spawnDirection) * (Math.random() * displacementX));
        float posy = y + (float) (Math.cos(spawnDirection) * (Math.random() * displacementY));

        p.setX(posx);
        p.setY(posy);
        p.setTime((int) (Math.random() * (maxDuration - minDuration) + minDuration), fadeOut);
        p.initSprite(resourceProvider.getTexture(picture));

        float neuDirection = (float) (spawnDirection + Math.random() * angleRange - angleRange / 2);
        float spx = (float) (Math.sin(neuDirection) * (Math.random() * (maxSpeed - minSpeed) + minSpeed));
        float spy = (float) (Math.cos(neuDirection) * (Math.random() * (maxSpeed - minSpeed) + minSpeed));
        p.setSx(spx);
        p.setSy(spy);
        p.setGx(gravityX);
        p.setGy(gravityY);
        p.setFx(frictionX);
        p.setFy(frictionY);
        p.setRotationSpeed((float) (Math.random() * rotationSpeed * 2) - rotationSpeed);
        p.setSize(size, size);
        p.setColor(color);
    }

    public ParticleExplosion setX(float x) {
        this.x = x;
        return this;
    }

    public ParticleExplosion setY(float y) {
        this.y = y;
        return this;
    }

    public ParticleExplosion setRotationSpeed(float rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
        return this;
    }

    public ParticleExplosion setAmount(float amount) {
        this.amount = amount;
        return this;
    }

    public ParticleExplosion setDisplacementX(float displacementX) {
        this.displacementX = displacementX;
        return this;
    }

    public ParticleExplosion setDisplacementY(float displacementY) {
        this.displacementY = displacementY;
        return this;
    }

    public ParticleExplosion setAngleRange(float angleRange) {
        this.angleRange = angleRange;
        return this;
    }

    public ParticleExplosion setMinSpeed(float minSpeed) {
        this.minSpeed = minSpeed;
        return this;
    }

    public ParticleExplosion setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
        return this;
    }

    public ParticleExplosion setGravityX(float gravityX) {
        this.gravityX = gravityX;
        return this;
    }

    public ParticleExplosion setGravityY(float gravityY) {
        this.gravityY = gravityY;
        return this;
    }

    public ParticleExplosion setPicture(String picture) {
        this.picture = picture;
        return this;
    }

    public ParticleExplosion setMinDuration(int minDuration) {
        this.minDuration = minDuration;
        return this;
    }

    public ParticleExplosion setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
        return this;
    }

    public ParticleExplosion setSize(float size) {
        this.size = size;
        return this;
    }

    public ParticleExplosion setFadeOut(boolean fadeOut) {
        this.fadeOut = fadeOut;
        return this;
    }

    public ParticleExplosion setColor(Color color) {
        this.color = color;
        return this;
    }

    public ParticleExplosion setFrictionX(float frictionX) {
        this.frictionX = frictionX;
        return this;
    }

    public ParticleExplosion setFrictionY(float frictionY) {
        this.frictionY = frictionY;
        return this;
    }

}
