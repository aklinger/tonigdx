package com.aa.tonigdx.logic.particle;

/**
 * A Particle that gets pulled to a point by gravity.
 *
 * @author Toni
 */
public class GravityParticle extends SimpleParticle {
    private float targetx;
    private float targety;
    private float strength;
    private boolean reachedTarget;
    private boolean linearGravity;

    @Override
    public GravityParticle copy() {
        GravityParticle gravityParticle = new GravityParticle();

        GravityParticle.copyState(this, gravityParticle);

        return gravityParticle;
    }

    protected static void copyState(GravityParticle original, GravityParticle duplicate) {
        SimpleParticle.copyState(original, duplicate);

        duplicate.targetx = original.targetx;
        duplicate.targety = original.targety;
        duplicate.strength = original.strength;
        duplicate.reachedTarget = original.reachedTarget;
        duplicate.linearGravity = original.linearGravity;
    }

    @Override
    public void act() {
        float dx = targetx - this.x;
        float dy = targety - this.y;
        float dist = (float) Math.sqrt(dx * dx + dy * dy);
        if (linearGravity) {
            this.setGx(dx * strength / (dist));
            this.setGy(dy * strength / (dist));
        } else {
            this.setGx(dx * strength / (dist * dist));
            this.setGy(dy * strength / (dist * dist));
        }

        super.act();

        if (Math.abs(dx) < 30 && Math.abs(dy) < 30) {
            reachedTarget = true;
        }
    }

    public boolean hasReachedTarget() {
        return reachedTarget;
    }

    @Override
    public boolean isActive() {
        return !reachedTarget;
    }

    public void setLinearGravity(boolean linearGravity) {
        this.linearGravity = linearGravity;
    }

    public void setStrength(float strength) {
        this.strength = strength;
    }

    public void setTargetx(float targetx) {
        this.targetx = targetx;
    }

    public void setTargety(float targety) {
        this.targety = targety;
    }

}
