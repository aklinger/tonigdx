package com.aa.tonigdx.logic.particle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;

/**
 * An abstract representation of a Class that manages Particles. (Stuff that can
 * only look pretty)
 * <p/>
 * The implementation that is the fastest in the specific needed case should
 * then be chosen (or make it yourself).
 * <p/>
 *
 * @author Toni
 */
public interface ParticleManager extends Iterable<Particle> {
    /**
     * Adds the Particle to the Manager
     * <p/>
     *
     * @param particle The Particle to add.
     */
    void addParticle(Particle particle);

    /**
     * May add all Particles in the Collection to the Manager
     * <p/>
     *
     * @param particles The Particles to add to the Manager.
     */
    void addAllParticles(Array<Particle> particles);

    /**
     * Is called once every frame. Updates all the Particles and manages them.
     */
    void act();

    /**
     * Renders all the Particles.
     */
    void render(Batch batch);

    /**
     * Removes all Particles.
     */
    void clear();
}
