package com.aa.tonigdx.logic.particle;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * A Particle. (Something that looks pretty)
 * <p/>
 *
 * @author Toni
 */
public interface Particle {
    /**
     * That what should happen every frame.
     */
    void act();

    /**
     * Renders the Particle.
     *
     * @param batch The Batch to use for rendering.
     */
    void render(Batch batch);

    /**
     * Creates a deep copy of this particle.
     *
     * @return A copy of this particle.
     */
    Particle copy();

    /**
     * If the Particle is still active.
     *
     * @return False if the Particle can be removed.
     */
    boolean isActive();

    void free();
}