package com.aa.tonigdx.logic.particle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

/**
 * Manages a lot of Particles (stuff that only looks pretty) with an ArrayList.
 * <p/>
 * It maybe doesn't do this the best way, but if it's a bottleneck, you can
 * write your own implementation.
 * <p/>
 *
 * @author Toni
 */
public class ParticleList implements ParticleManager {

    private final Array<Particle> particles;
    private final Array<Particle> trash;

    /**
     * Creates a new ParticleList using a simple ArrayList.
     */
    public ParticleList() {
        particles = new Array<>();
        trash = new Array<>();
    }

    @Override
    public void addParticle(Particle particle) {
        particles.add(particle);
    }

    @Override
    public void addAllParticles(Array<Particle> particles) {
        this.particles.addAll(particles);
    }

    @Override
    public void act() {
        for (Particle part : particles) {
            part.act();
            if (!part.isActive()) {
                trash.add(part);
            }
        }
        if (trash.size > 0) {
            particles.removeAll(trash, true);
            for (Particle p : trash) {
                p.free();
            }
        }
        trash.clear();
    }

    @Override
    public void render(Batch batch) {
        for (Particle part : particles) {
            part.render(batch);
        }
    }

    @Override
    public void clear() {
        for (Particle p : particles) {
            p.free();
        }
        particles.clear();
    }

    @Override
    public Iterator<Particle> iterator() {
        return particles.iterator();
    }
}
