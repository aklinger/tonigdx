package com.aa.tonigdx.logic.particle;

import com.aa.tonigdx.dal.ResourceProvider;

/**
 * A builder for explosions with the GravityParticle.
 *
 * @author Toni
 */
public class GravityParticleExplosion extends ParticleExplosion {
    private float targetx;
    private float targety;
    private float strenght;
    private boolean linearGravity;

    @Override
    protected SimpleParticle getInstanceOfNewParticle() {
        return new GravityParticle();
    }

    @Override
    protected void initParticle(SimpleParticle p, ResourceProvider resourceProvider) {
        super.initParticle(p, resourceProvider);
        GravityParticle gp = (GravityParticle) p;
        gp.setTargetx(targetx);
        gp.setTargety(targety);
        gp.setStrength(strenght);
        gp.setLinearGravity(linearGravity);
    }


    public GravityParticleExplosion setTargetx(float targetx) {
        this.targetx = targetx;
        return this;
    }

    public GravityParticleExplosion setTargety(float targety) {
        this.targety = targety;
        return this;
    }

    public GravityParticleExplosion setStrenght(float strenght) {
        this.strenght = strenght;
        return this;
    }

    public GravityParticleExplosion setLinearGravity(boolean linearGravity) {
        this.linearGravity = linearGravity;
        return this;
    }
}
