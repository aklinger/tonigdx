package com.aa.tonigdx.logic.particle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * A lightWeight implementation of a Particle.
 * <p/>
 * It only has what it needs.
 * <p/>
 *
 * @author Toni
 */
public class SimpleParticle implements Particle, Pool.Poolable {
    protected float x;
    protected float y;
    protected float sx;
    protected float sy;
    protected float fx = 1;
    protected float fy = 1;
    protected float gx;
    protected float gy;
    protected float rotation;
    protected float rotationSpeed;
    protected float rotationFriction;
    protected float rotationGravitation;

    protected int time;
    protected float alpha = 1f;
    protected float alphaReduction;

    protected Sprite sprite;

    protected Pool owner;

    public SimpleParticle() {
        sprite = new Sprite();
    }

    public void initSprite(TextureRegion texture) {
        sprite.setRegion(texture);
        sprite.setSize(texture.getRegionWidth(), texture.getRegionHeight());
        sprite.setOrigin(texture.getRegionWidth() / 2, texture.getRegionHeight() / 2);
    }

    public SimpleParticle copy() {
        SimpleParticle simpleParticle = new SimpleParticle();

        copyState(this, simpleParticle);

        return simpleParticle;
    }

    protected static void copyState(SimpleParticle original, SimpleParticle duplicate) {
        duplicate.sprite = original.sprite;

        duplicate.x = original.x;
        duplicate.y = original.y;
        duplicate.sx = original.sx;
        duplicate.sy = original.sy;
        duplicate.fx = original.fx;
        duplicate.fy = original.fy;
        duplicate.gx = original.gx;
        duplicate.gy = original.gy;
        duplicate.rotation = original.rotation;
        duplicate.rotationSpeed = original.rotationSpeed;
        duplicate.rotationFriction = original.rotationFriction;
        duplicate.rotationGravitation = original.rotationGravitation;

        duplicate.time = original.time;
        duplicate.alpha = original.alpha;
        duplicate.alphaReduction = original.alphaReduction;
    }


    @Override
    public void reset() {
        x = 0;
        y = 0;
        sx = 0;
        sy = 0;
        fx = 1;
        fy = 1;
        gx = 0;
        gy = 0;
        rotation = 0;
        rotationSpeed = 0;
        rotationFriction = 0;
        rotationGravitation = 0;
        time = 0;
        alpha = 1;
        alphaReduction = 0;
    }


    @Override
    public void act() {
        calcMovement();
        calcRotation();
        calcFadeout();
    }

    private void calcMovement() {
        sx = (sx * fx) + gx;
        sy = (sy * fy) + gy;
        x += sx;
        y += sy;
    }

    private void calcRotation() {
        rotationSpeed = (rotationSpeed * rotationFriction) + rotationGravitation;
        rotation += rotationSpeed;
        rotationSpeed %= 360;
        rotation %= 360;
    }

    private void calcFadeout() {
        time--;
        alpha -= alphaReduction;
        if (alpha < 0) {
            alpha = 0;
        }
    }

    @Override
    public void render(Batch batch) {
        sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
        sprite.setRotation(rotation);
        sprite.setPosition(x, y);
        Color c = sprite.getColor();
        sprite.setColor(c.r, c.g, c.b, alpha);
        sprite.draw(batch);
    }

    @Override
    public boolean isActive() {
        return time >= 0 && alpha >= 0;
    }

    @Override
    public void free() {
        if (owner != null) {
            owner.free(this);
        }
    }

    public void setOwner(Pool owner) {
        this.owner = owner;
    }

    public Pool getOwner() {
        return owner;
    }

    public void setCenter(Vector2 position) {
        x = position.x - sprite.getWidth() / 2;
        y = position.y - sprite.getHeight() / 2;
    }

    public void setFx(float fx) {
        this.fx = fx;
    }

    public void setFy(float fy) {
        this.fy = fy;
    }

    public void setGx(float gx) {
        this.gx = gx;
    }

    public void setGy(float gy) {
        this.gy = gy;
    }

    public void setSx(float sx) {
        this.sx = sx;
    }

    public void setSy(float sy) {
        this.sy = sy;
    }

    public void setTime(int time, boolean fadeOut) {
        this.time = time;
        if (fadeOut) {
            alphaReduction = alpha / time;
        } else {
            alphaReduction = 0;
        }
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setRotationSpeed(float rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }

    public void setSize(float width, float height) {
        this.sprite.setSize(width, height);
    }

    public void setColor(Color color) {
        sprite.setColor(color);
    }

    public void setRotationFriction(float rotationFriction) {
        this.rotationFriction = rotationFriction;
    }

    public void setRotationGravitation(float rotationGravitation) {
        this.rotationGravitation = rotationGravitation;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getRotation() {
        return rotation;
    }
}
