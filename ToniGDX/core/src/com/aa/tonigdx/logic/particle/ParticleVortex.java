package com.aa.tonigdx.logic.particle;

import com.badlogic.gdx.utils.Array;

/**
 * Manages a complex animation of multiple particles.
 *
 * @author Toni
 */
public class ParticleVortex {
    private final Array<GravityParticle> particles;

    public ParticleVortex() {
        particles = new Array<>();
    }

    public void addParticle(GravityParticle p) {
        particles.add(p);
    }

    public boolean isFinished() {
        for (GravityParticle gp : particles) {
            if (!gp.hasReachedTarget()) {
                return false;
            }
        }
        return true;
    }

    public Array<GravityParticle> getParticles() {
        return particles;
    }
}
