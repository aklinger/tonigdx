package com.aa.tonigdx.logic.pattern;

/**
 * Created by Toni on 01.06.2015.
 */
public class PatternDefinition<Type> {

    private Type pattern;
    private int minPatternRepeatDistance;
    private int maxPatternRepeatDistance;
    private float patternRepeatLikelihood;
    private int maxStartDelay;

    private static final long NOT_USED = Integer.MIN_VALUE;
    private long lastTimePatternUsed = NOT_USED;

    public PatternDefinition(Type pattern, int minPatternRepeatDistance, int maxPatternRepeatDistance, float patternRepeatLikelihood) {
        this(pattern, minPatternRepeatDistance, maxPatternRepeatDistance, patternRepeatLikelihood, maxPatternRepeatDistance);
    }

    public PatternDefinition(Type pattern, int minPatternRepeatDistance, int maxPatternRepeatDistance, float patternRepeatLikelihood, int maxStartDelay) {
        this.pattern = pattern;
        this.minPatternRepeatDistance = minPatternRepeatDistance;
        this.maxPatternRepeatDistance = maxPatternRepeatDistance;
        this.patternRepeatLikelihood = Math.max(patternRepeatLikelihood, 1);
        this.maxStartDelay = maxStartDelay;
    }

    public void reset() {
        lastTimePatternUsed = NOT_USED;
    }

    public Type getPattern() {
        return pattern;
    }

    public boolean isViable(long currentIndex) {
        return currentIndex - lastTimePatternUsed > minPatternRepeatDistance;
    }

    public boolean isNecessary(long currentIndex) {
        if (lastTimePatternUsed < 0) {
            return currentIndex >= maxPatternRepeatDistance || currentIndex >= maxStartDelay;
        } else {
            return currentIndex - lastTimePatternUsed > maxPatternRepeatDistance;
        }
    }

    public float getPatternRepeatLikelihood() {
        return patternRepeatLikelihood;
    }

    public void setLastTimePatternUsed(long lastTimePatternUsed) {
        this.lastTimePatternUsed = lastTimePatternUsed;
    }
}
