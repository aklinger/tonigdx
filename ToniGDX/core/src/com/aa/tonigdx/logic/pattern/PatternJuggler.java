package com.aa.tonigdx.logic.pattern;

import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by Toni on 01.06.2015.
 */
public class PatternJuggler<Type> {

    private Array<PatternDefinition<Type>> patterns;
    private PatternDefinition<Type> currentPattern;
    private long patternIndex;
    private Random random;

    public PatternJuggler(Random random) {
        patterns = new Array<>();
        this.random = random;
    }

    public void reset() {
        patternIndex = 0;
        currentPattern = null;
        for (PatternDefinition patternDefinition : patterns) {
            patternDefinition.reset();
        }
    }

    public void addPattern(PatternDefinition<Type> pattern) {
        patterns.add(pattern);
    }

    public void addPattern(Type pattern, int minPatternRepeatDistance, int maxPatternRepeatDistance, int patternRepeatLikelihood) {
        patterns.add(new PatternDefinition<>(pattern, minPatternRepeatDistance, maxPatternRepeatDistance, patternRepeatLikelihood));
    }

    public void addPattern(Type pattern, int minPatternRepeatDistance, int maxPatternRepeatDistance, int patternRepeatLikelihood, int maxStartDelay) {
        patterns.add(new PatternDefinition<>(pattern, minPatternRepeatDistance, maxPatternRepeatDistance, patternRepeatLikelihood, maxStartDelay));
    }

    public Type nextPattern() {
        currentPattern = chooseNextPattern();
        patternIndex++;
        if (currentPattern != null) {
            currentPattern.setLastTimePatternUsed(patternIndex);
            return currentPattern.getPattern();
        } else {
            return null;
        }
    }

    private transient Array<PatternDefinition<Type>> viablePatterns = new Array<>();

    private PatternDefinition<Type> chooseNextPattern() {
        boolean onlyCritical = false;
        int likelihoodSum = 0;
        viablePatterns.clear();
        for (PatternDefinition<Type> patternDef : patterns) {
            if (patternDef.isNecessary(patternIndex)) {
                if (!onlyCritical) {
                    likelihoodSum = 0;
                    viablePatterns.clear();
                    onlyCritical = true;
                }

                viablePatterns.add(patternDef);
                likelihoodSum += patternDef.getPatternRepeatLikelihood();
            } else if (!onlyCritical && patternDef.isViable(patternIndex)) {
                viablePatterns.add(patternDef);
                likelihoodSum += patternDef.getPatternRepeatLikelihood();
            }
        }
        if (viablePatterns.size != 0) {
            int counter = random.nextInt(likelihoodSum) + 1;

            for (PatternDefinition<Type> patternDef : viablePatterns) {
                counter -= patternDef.getPatternRepeatLikelihood();
                if (counter <= 0) {
                    return patternDef;
                }
            }
            return viablePatterns.get(0);
        }
        return null;
    }

    public PatternDefinition<Type> getCurrentPatternDefinition() {
        return currentPattern;
    }

    public Type getCurrentPattern() {
        return currentPattern.getPattern();
    }

    public Array<PatternDefinition<Type>> getPatterns() {
        return patterns;
    }
}
