package com.aa.tonigdx.logic.physics;

import com.aa.tonigdx.maths.Collision;

/**
 * Created by toni on 6/23/16.
 */
public interface CollisionResolver {

    /**
     * This Method resolves a Collision so that two overlapping Objects do not
     * overlap anymore.
     * <p/>
     * This Method does not check for null anymore, that has to be done
     * beforehand.
     * <p/>
     * @param col The Collision that has occurred.
     */
    void resolveCollision(Collision col, boolean exertForce);
}
