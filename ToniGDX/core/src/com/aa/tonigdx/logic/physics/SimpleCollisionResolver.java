package com.aa.tonigdx.logic.physics;

import com.aa.tonigdx.logic.Entity;
import com.aa.tonigdx.maths.Collision;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by toni on 6/26/16.
 */
public class SimpleCollisionResolver implements CollisionResolver {

    @Override
    public void resolveCollision(Collision col, boolean exertForce) {
        Vector2 displacementRelativeTo = col.getDisplacementRelativeTo(col.getMain());
        col.getMain().addPosition(displacementRelativeTo);

        stopVelocity(col.getMain(), displacementRelativeTo);
    }

    public static void stopVelocity(Entity entity, Vector2 displacement){
        Vector2 speed = entity.getSpeed();
        if(speed.x < 0 && displacement.x > 0){
            speed.x = 0;
        }else if(speed.x > 0 && displacement.x < 0){
            speed.x = 0;
        }
        if(speed.y < 0 && displacement.y > 0){
            speed.y = 0;
        }else if(speed.y > 0 && displacement.y < 0){
            speed.y = 0;
        }
        entity.setSpeed(speed);
    }
}
