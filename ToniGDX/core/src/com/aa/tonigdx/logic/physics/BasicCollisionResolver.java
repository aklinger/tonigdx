package com.aa.tonigdx.logic.physics;

import com.aa.tonigdx.logic.Entity;
import com.aa.tonigdx.maths.Collision;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by toni on 6/23/16.
 */
public class BasicCollisionResolver implements CollisionResolver{

    private static final boolean staticObjectsPushEachOther = false;

    /**
     * This Method resolves a Collision so that two overlapping Objects do not
     * overlap anymore.
     * <p/>
     * This Method does not check for null anymore, that has to be done
     * beforehand.
     * <p/>
     * @param col The Collision that has occurred.
     */
    public void resolveCollision(Collision col, boolean exertForce) {
        Entity obj1 = col.getBox1().getParent();
        Entity obj2 = col.getBox2().getParent();
        Vector2 displacement;
        if (!obj1.isFixated() && !obj2.isFixated()) {
            float totalMass = obj1.getMass() + obj2.getMass();
            float factor1 = obj2.getMass() / totalMass;
            float factor2 = obj1.getMass() / totalMass;
            displacement = col.getDisplacement().cpy().scl(factor1,factor1);
            obj1.addPosition(displacement);
            if(exertForce){
                obj1.addSpeed(displacement.scl(obj1.getBounciness()));
            }
            displacement = col.getDisplacement().cpy().scl(-factor2);
            obj2.addPosition(displacement);
            if(exertForce){
                obj2.addSpeed(displacement.scl(obj2.getBounciness()));
            }
        } else if (!obj1.isFixated()) {
            displacement = col.getDisplacement().cpy();
            obj1.addPosition(displacement);
            if(exertForce){
                obj1.addSpeed(displacement.scl(obj1.getBounciness()));
            }
        } else if (!obj2.isFixated()) {
            displacement = col.getDisplacement().cpy().scl(-1);
            obj2.addPosition(displacement);
            if(exertForce){
                obj2.addSpeed(displacement.scl(obj2.getBounciness()));
            }
        } else if(staticObjectsPushEachOther){
            //This else will allow two static objects to push each other.
            displacement = col.getDisplacement().scl(0.5f);
            obj1.addPosition(displacement);
            displacement = displacement.scl(-1);
            obj2.addPosition(displacement);
        }
    }
}
