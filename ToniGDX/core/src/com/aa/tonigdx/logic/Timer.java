package com.aa.tonigdx.logic;

/**
 * Created by Toni on 15.04.2015.
 */
public class Timer {
    private float timer;
    private float duration;

    public Timer(float duration, boolean immediatelyReady) {
        this.duration = duration;
        this.timer = immediatelyReady ? duration : 0;
    }

    public void advanceTimer(float delta) {
        timer += delta;
    }

    public boolean checkTimer() {
        return timer >= duration;
    }

    public boolean advanceAndCheckTimer(float delta) {
        advanceTimer(delta);
        return checkTimer();
    }

    public void resetTimer() {
        timer = 0;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public void setTimer(float timer) {
        this.timer = timer;
    }

    public float getDuration() {
        return duration;
    }

    public float getTimeLeft() {
        return Math.max(duration - timer, 0);
    }

    public float getPercentOfTimeLeft() {
        return getTimeLeft() / getDuration();
    }

    public float getTimeProgress() {
        return timer;
    }
}
