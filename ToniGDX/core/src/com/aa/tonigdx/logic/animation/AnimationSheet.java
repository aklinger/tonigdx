package com.aa.tonigdx.logic.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by Toni on 15.04.2014.
 */
public class AnimationSheet {

    private float time;
    private String currentAnimationName;
    private ObjectMap<String, Animation<TextureRegion>> animations;
    private String followupAnimationName;

    public static final String ANIMATION_DEFAULT = "default";

    public AnimationSheet() {
        this(ANIMATION_DEFAULT);
    }

    public AnimationSheet(String currentAnimationName) {
        this.currentAnimationName = currentAnimationName;

        animations = new ObjectMap<>();
        time = 0;
    }

    private AnimationSheet(AnimationSheet template) {
        this.currentAnimationName = template.currentAnimationName;
        this.animations = template.animations;
        this.time = template.time;
    }

    public AnimationSheet shallowCopy() {
        return new AnimationSheet(this);
    }


    public void act(float delta) {
        time += delta;
        if (followupAnimationName != null) {
            if (isAnimationFinished()) {
                setCurrentAnimation(followupAnimationName, true);
                followupAnimationName = null;
            }
        }
    }

    public TextureRegion getCurrentFrame() {
        return animations.get(currentAnimationName).getKeyFrame(time);
    }

    public void putTextureRegion(String name, TextureRegion region) {
        Animation<TextureRegion> anim = new Animation<>(1, region);
        putAnimation(name, anim);
    }

    public void putAnimation(String name, Animation<TextureRegion> anim) {
        animations.put(name, anim);
    }

    public boolean isAnimationFinished() {
        return animations.get(currentAnimationName).isAnimationFinished(time);
    }

    public void rewindCurrentAnimation() {
        this.time = 0;
    }

    public void setFrame(int frame) {
        time = animations.get(currentAnimationName).getFrameDuration() * frame;
    }

    public void setCurrentAnimation(String currentAnimationName) {
        setCurrentAnimation(currentAnimationName, false);
    }

    public void setCurrentAnimation(String currentAnimation, boolean rewind) {
        this.currentAnimationName = currentAnimation;
        if (!animations.containsKey(currentAnimation)) {
            Gdx.app.debug("AnimationSheet", "Missing animation '" + currentAnimation + "', using default instead.");
            this.currentAnimationName = ANIMATION_DEFAULT;
        }
        if (rewind) {
            rewindCurrentAnimation();
        }
    }

    public void setCurrentAnimationFollowupLoop(String currentAnimation, String followupAnimationName) {
        setCurrentAnimation(currentAnimation, true);
        this.followupAnimationName = followupAnimationName;
    }

    public void setFollowupAnimation(String followupAnimationName){
        this.followupAnimationName = followupAnimationName;
    }

    public String getFollowupAnimationName() {
        return followupAnimationName;
    }

    public Animation<TextureRegion> getCurrentAnimation() {
        return animations.get(currentAnimationName);
    }

    public String getCurrentAnimationName() {
        return currentAnimationName;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public Animation<TextureRegion> getAnimation(String name) {
        return animations.get(name);
    }

    public int getFrameIndex() {
        return animations.get(currentAnimationName).getKeyFrameIndex(time);
    }

    public TextureRegion getFrameFromAnimation(String name) {
        return animations.get(name).getKeyFrame(time);
    }

    public TextureRegion getFrameFromAnimation(String name, float time) {
        return animations.get(name).getKeyFrame(time);
    }

    public float getTime() {
        return time;
    }
}
