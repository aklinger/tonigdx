package com.aa.tonigdx.util;

import com.aa.tonigdx.maths.Calc;
import com.badlogic.gdx.graphics.Color;

/**
 * All the Utility Methods you need for Colors.
 * <p/>
 * @author Toni
 */
public class ColorUtil {
    //Constructors
    private ColorUtil() {
    }

    //Methods
    public static Color randomColor(float minR, float minG, float minB, float minA) {
        return new Color(minR + (float) (Math.random() * (1 - minR)), minG + (float) (Math.random() * (1 - minG)), minB + (float) (Math.random() * (1 - minB)), minA + (float) (Math.random() * (1 - minA)));
    }

    public static Color randomColor(float minR, float minG, float minB) {
        return new Color(minR + (float) (Math.random() * (1 - minR)), minG + (float) (Math.random() * (1 - minG)), minB + (float) (Math.random() * (1 - minB)), 1f);
    }

    public static Color randomColor(float alpha) {
        return new Color((float) (Math.random()), (float) (Math.random()), (float) (Math.random()), alpha);
    }

    public static Color invertedColor(Color color) {
        return new Color(1f - color.r, 1f - color.g, 1f - color.b, color.a);
    }

    public static Color jumbleColor(Color source, float displacement){
        return new Color((float) Calc.fastRange((Math.random() * displacement * 2 - displacement) + source.r, 0, 1),(float)Calc.fastRange((Math.random()*displacement*2-displacement)+source.g,0,1),(float)Calc.fastRange((Math.random()*displacement*2-displacement)+source.b,0,1),source.a);
    }
}
