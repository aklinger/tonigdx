package com.aa.tonigdx.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

/**
 * An Util class for logging.
 * <p/>
 * @author Toni
 */
public class LogUtil {
    //Attribute
    public static final String DEBUG = "DEBUG";
    public static final String INFO = "INFO";
    public static final String WARNING = "WARNING";
    public static final String ERROR = "ERROR";
    public static boolean debug = false;
    
    //Konstruktor
    private LogUtil() {
    }
    
    //Methoden
    public static boolean isDebug() {
        return LogUtil.debug;
    }

    public static void setDebug(boolean debug) {
        LogUtil.debug = debug;
        if (debug) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        } else {
            Gdx.app.setLogLevel(Application.LOG_INFO);
        }
    }

    public static void log(String message) {
        Gdx.app.log(INFO, message);
    }

    public static void log(String message, Exception ex) {
        Gdx.app.log(INFO, message, ex);
    }

    public static void debug(String message) {
        Gdx.app.debug(DEBUG, message);
    }

    public static void debug(String message, Exception ex) {
        Gdx.app.log(DEBUG, message, ex);
    }

    public static void warn(String message) {
        Gdx.app.log(WARNING, message);
    }

    public static void warn(String message, Exception ex) {
        Gdx.app.log(WARNING, message, ex);
    }

    public static void error(String message) {
        Gdx.app.error(ERROR, message);
    }

    public static void error(String message, Exception ex) {
        Gdx.app.error(INFO, message, ex);
    }
}
