package com.aa.tonigdx;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import com.aa.tonigdx.dal.AnimationsLoader;
import com.aa.tonigdx.dal.TextureManager;
import com.aa.tonigdx.dal.input.ControlSettings;
import com.aa.tonigdx.dal.input.remap.Remapper;
import com.aa.tonigdx.gui.LazySkin;
import com.aa.tonigdx.gui.ScreenMultiplexer;
import com.aa.tonigdx.logic.GameManager;
import com.aa.tonigdx.logic.tween.ActorAccessor;
import com.aa.tonigdx.logic.tween.LabelAccessor;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ToniGDXGameBase extends ApplicationAdapter {

    private ToniGDXGameConfig gameConfig;

    private Batch batch;
    private Skin skin;
    private Color clearColor;
    private TweenManager tweenManager;

    private ScreenMultiplexer hud;
    private GameManager gameManager;

    public ToniGDXGameBase(ToniGDXGameConfig gameConfig) {
        this.gameConfig = gameConfig;
    }

    @Override
    public void create() {
        initGraphics(gameConfig);
        initGame(gameConfig);
        initInput();
        initAudio();
    }

    protected void initGraphics(ToniGDXGameConfig gameConfig) {
        this.clearColor = gameConfig.getClearColor();
        this.batch = new SpriteBatch();
        this.skin = initSkin(gameConfig);
        TextureManager.getInstance().loadTexturesFromAtlas(gameConfig.getTextureAtlasFilePath());
        AnimationsLoader.getInstance().cacheAnimationSheets(Gdx.files.internal(gameConfig.getAnimationsFilePath()));

        tweenManager = new TweenManager();
        Tween.registerAccessor(Actor.class, new ActorAccessor());
        Tween.registerAccessor(Label.class, new LabelAccessor());
        Tween.setCombinedAttributesLimit(4);
    }

    protected Skin initSkin(ToniGDXGameConfig gameConfig) {
        return new LazySkin(Gdx.files.internal(gameConfig.getSkinFilePath()));
    }

    protected void registerResizedVersionOfFontInSkin(Skin skin, BitmapFont font, float scale, String smallFontName) {
        font.getData().scaleX = scale;
        font.getData().scaleY = scale;
        skin.add(smallFontName, font);
    }

    protected void initGame(ToniGDXGameConfig gameConfig) {

    }

    protected void initInput() {
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        addProcessorsToInputMultiplexer(inputMultiplexer);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    protected void addProcessorsToInputMultiplexer(InputMultiplexer inputMultiplexer) {
        inputMultiplexer.addProcessor(Remapper.getInstance());
        inputMultiplexer.addProcessor(ControlSettings.getInstance());
        inputMultiplexer.addProcessor(hud);
        inputMultiplexer.addProcessor(gameManager);
    }

    protected void initAudio() {

    }

    @Override
    public void render() {
        actFrame();
        actInput();

        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameManager.render(batch);
        hud.renderStages();
    }

    protected void actFrame() {
        gameManager.actFrame();
        hud.actStages();
    }

    protected void actInput() {
        ControlSettings.getInstance().actFrame();
    }

    @Override
    public void resize(int width, int height) {
        gameManager.resize(width, height);
        hud.updateViewportSize(width, height);
    }

    public Skin getSkin() {
        return skin;
    }

    public Batch getBatch() {
        return batch;
    }

    public Color getClearColor() {
        return clearColor;
    }

    public void setClearColor(Color clearColor) {
        this.clearColor = clearColor;
    }

    public ScreenMultiplexer getHud() {
        return hud;
    }

    public TweenManager getTweenManager() {
        return tweenManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public void setGameManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    public void setHud(ScreenMultiplexer hud) {
        this.hud = hud;
    }
}
