package com.aa.tonigdx.maths;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * My own version of the java.lang.Math class. Everything that should be there
 * gets added here.
 * <p/>
 * If a Method is prefixed "fast", it often trades accuracy or error-security
 * for speed.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class Calc {
    /**
     * A complete Circle in Radians.
     */
    public static final double TWO_PI = Math.PI * 2;

    /**
     * This is a utility-class, therefore no instancing.
     */
    private Calc() {
    }

    /**
     * Faster implementation than Math.floor()
     * <p/>
     * Ignores cases like when value is infinity.
     * <p/>
     *
     * @param number The number to round down.
     * @return The next Integer that is smaller or equal to number.
     */
    public static int fastFloor(double number) {
        return (number < 0) ? (number == (int) number) ? (int) number : (int) (number - 1) : (int) number;
    }

    /**
     * Faster implementation than Math.ceil().
     * <p/>
     * Ignores cases like when value is infinity.
     * <p/>
     *
     * @param number The number to round up.
     * @return The next Integer that is greater or equal to number.
     */
    public static int fastCeil(double number) {
        return (number == (int) (number)) ? (int) number : (number < 0) ? (int) number : (int) (number + 1);
    }

    /**
     * Faster implementation than Math.max() for double values.
     * <p/>
     * Ignores cases like when values are infinity.
     * <p/>
     *
     * @param a The first number
     * @param b The second number
     * @return The larger Number of both arguments.
     */
    public static double fastMax(double a, double b) {
        return (a >= b) ? a : b;
    }

    /**
     * Faster implementation than Math.min() for double values.
     * <p/>
     * Ignores cases like when values are infinity.
     * <p/>
     *
     * @param a The first number
     * @param b The second number
     * @return The smaller Number of both arguments.
     */
    public static double fastMin(double a, double b) {
        return (a <= b) ? a : b;
    }

    /**
     * Ensures that the returned value is within a certain range.
     * <p/>
     * Uses fastMin() and fastMax().
     * <p/>
     *
     * @param value  The value to fit into the range.
     * @param range1 The first constraint of the range.
     * @param range2 The second constraint of the range.
     * @return A value that is within the range.
     */
    public static double fastRange(double value, double range1, double range2) {
        if (range1 < range2) {
            return fastMin(range2, fastMax(range1, value));
        } else {
            return fastMin(range1, fastMax(range2, value));
        }
    }

    /**
     * Squares the input value.
     * <p/>
     *
     * @param a The value to square
     * @return The value squared (a*a)
     */
    public static double square(double a) {
        return a * a;
    }

    public static double randomValueInRange(double min, double max){
        return Math.random()*(max-min)+min;
    }

    public static int randomSign(){
        return Math.random() < 0.5f ? -1 : 1;
    }

    public static Rectangle createRectangleFromPoints(Vector2 point1, Vector2 point2){
        float x1,x2,y1,y2;
        if(point1.x < point2.x){
            x1 = point1.x;
            x2 = point2.x;
        }else{
            x1 = point2.x;
            x2 = point1.x;
        }
        if(point1.y < point2.y){
            y1 = point1.y;
            y2 = point2.y;
        }else{
            y1 = point2.y;
            y2 = point1.y;
        }
        return new Rectangle(x1,y1,x2-x1,y2-y1);
    }
}
