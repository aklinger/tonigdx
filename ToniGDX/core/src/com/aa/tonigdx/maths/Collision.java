package com.aa.tonigdx.maths;

import com.aa.tonigdx.logic.Entity;
import com.badlogic.gdx.math.Vector2;

/**
 * Saves all important information about a HitBox-Collision.
 * <p/>
 * @author Toni
 */
public class Collision {
    //Attribute
    private Entity main;
    private HitBox box1;
    private HitBox box2;
    private Vector2 displacement;
    private Vector2 collision;
    //Konstruktor
    public Collision(HitBox box1, HitBox box2, Vector2 displacement, Vector2 collision) {
        this.box1 = box1;
        this.box2 = box2;
        this.displacement = displacement;
        this.collision = collision;
    }
    //Getter & Setter
    public HitBox getBox1() {
        return box1;
    }
    public HitBox getBox2() {
        return box2;
    }
    public Vector2 getCollisionPoint() {
        return collision;
    }
    public void setCollision(Vector2 collision) {
        this.collision = collision;
    }
    public Vector2 getDisplacementRelativeTo(Entity obj){
    	if(obj == box1.getParent()){
    		return displacement.cpy();
    	}else{
    		return displacement.cpy().scl(-1);
    	}
    }
    public Vector2 getDisplacement() {
        return displacement;
    }
    public void setDisplacement(Vector2 displacement) {
        this.displacement = displacement;
    }
    public Entity getOtherHitBox(Entity obj) {
        if (box1.getParent() == obj) {
            return box2.getParent();
        } else {
            return box1.getParent();
        }
    }

    public void setMain(Entity main) {
        this.main = main;
    }

    public Entity getMain() {
        return main;
    }
}
