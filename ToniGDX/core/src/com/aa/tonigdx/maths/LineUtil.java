package com.aa.tonigdx.maths;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 23.08.2014.
 */
public class LineUtil {

    public static class CircleLineCollision {
        public Vector2 hit1;
        public Vector2 hit2;

        public boolean doesLineIntersectCircle() {
            return hit1 != null || hit2 != null;
        }

        public boolean doesLineHaveFirstIntersection() {
            return hit1 != null;
        }
    }

    // w = 2(n*v)n - v
    public static Vector2 getBounceAngle(Vector2 in, Vector2 normal) {
        in = in.cpy().nor().scl(-1);
        normal = normal.cpy().nor();

        return normal.scl((normal.dot(in)) * 2).sub(in);
    }

    public static float pointIsOnWhichSide(Vector2 lineA, Vector2 lineB, Vector2 point) {
        return Math.signum((lineB.x - lineA.x) * (point.y - lineA.y) - (lineB.y - lineA.y) * (point.x - lineA.x));
    }

    public static CircleLineCollision findIntersectionLineWithCircle(Vector2 rayStart, Vector2 rayEnd, Vector2 circleCenter, float radius) {
        Vector2 d = rayEnd.cpy().sub(rayStart);
        Vector2 f = rayStart.cpy().sub(circleCenter);

        float a = d.dot(d);
        float b = 2 * f.dot(d);
        float c = f.dot(f) - radius * radius;

        float discriminant = b * b - 4 * a * c;
        if (discriminant < 0) {
            //no intersection, something went wrong
            return new CircleLineCollision();
        } else {
            discriminant = (float) Math.sqrt(discriminant);

            float t1 = (-b - discriminant) / (2 * a);
            float t2 = (-b + discriminant) / (2 * a);

            CircleLineCollision clc = new CircleLineCollision();

            if (t1 >= 0 && t1 <= 1) {
                clc.hit1 = rayStart.cpy().add(d.cpy().scl(t1));
            }
            if (t2 >= 0 && t2 <= 1) {
                clc.hit2 = rayStart.cpy().add(d.scl(t2));
            }

            return clc;
        }
    }

    private static final Vector2 temp1 = new Vector2();
    private static final Vector2 temp2 = new Vector2();
    private static final Vector2 temp3 = new Vector2();

    /**
     * Finds the intersection between two lines.
     *
     * @param line1A first point on line1
     * @param line1B second point on line1
     * @param line2A first point on line2
     * @param line2B second point on line2
     * @return The intersection point of line1 and line2. Or null if the lines are parallel.
     */
    public static Vector2 findIntersectionLineWithLine(Vector2 line1A, Vector2 line1B, Vector2 line2A, Vector2 line2B) {
        Vector2 line1delta = temp1.set(line1B).sub(line1A);
        Vector2 line2delta = temp2.set(line2B).sub(line2A);

        if (line1delta.dot(line2delta) == 0) {
            //lines are parallel
            return null;
        } else {
            float t = (temp3.set(line2A).sub(line1A)).crs(line2delta.scl(1f / line1delta.crs(line2delta)));
            return temp3.set(line1A).add(line1delta.scl(t));
        }
    }

    public static boolean doesIntersect(Vector2 line1A, Vector2 line1B, Vector2 line2A, Vector2 line2B){
        float a = pointIsOnWhichSide(line1A, line1B, line2A);
        float b = pointIsOnWhichSide(line1A, line1B, line2B);
        float c = pointIsOnWhichSide(line2A, line2B, line1A);
        float d = pointIsOnWhichSide(line2A, line2B, line1B);
        return a != b && c != d;
    }
}
