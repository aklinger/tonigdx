package com.aa.tonigdx.maths;

import com.aa.tonigdx.logic.Entity;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * An interface to mark Classes as having Hitboxes.
 * <p/>
 *
 * @author Toni
 */
public interface HitBox {
    //Konstanten
    int AABB = 1;
    int CIRCLE = 2;

    //Abstrakte Methoden
    Entity getParent();

    Rectangle getBoundingBox();

    Vector2 getCenter();

    Vector2 getCenterReuse(Vector2 output);

    int getHitboxType();

    float getRadius();
}
