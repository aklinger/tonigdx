package com.aa.tonigdx.maths;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Utility-class for detecting collisions between HitBoxes (AABBs, Circles and
 * Lines).
 * <p/>
 *
 * @author Toni
 */
public class CollisionUtil {
    //Konstruktor

    /**
     * Utility-class no instancing.
     */
    private CollisionUtil() {
    }
    //Methoden

    /**
     * Calculates the Collision details of two HitBoxes.
     * <p/>
     *
     * @param box1         The first HitBox
     * @param box2         The second HitBox
     * @param displacement If the displacement needed to separate them again
     *                     should be calculated
     * @param point        If the central point of collision should be calculated
     * @return The Collision object with details or null if no Collision
     * occurred.
     */
    public static Collision calcCollisionHitBoxes(HitBox box1, HitBox box2, boolean displacement, boolean point) {
        if (box1.getHitboxType() == HitBox.CIRCLE) {
            if (box2.getHitboxType() == HitBox.CIRCLE) {
                return calcCollisionCircleVsCircle(box1, box2, displacement, point);
            } else {
                //Default is asuming instanceof AABB
                return calcCollisionAABBVsCircle(box2, box1, displacement, point);
            }
        } else {
            //Default is asuming instanceof AABB
            if (box2.getHitboxType() == HitBox.CIRCLE) {
                return calcCollisionAABBVsCircle(box1, box2, displacement, point);
            } else {
                //Default is asuming instanceof AABB
                return calcCollisionAABBVsAABB(box1, box2, displacement, point);
            }
        }
    }

    private static final Vector2 temp1 = new Vector2();
    private static final Vector2 temp2 = new Vector2();

    private static Collision calcCollisionAABBVsAABB(HitBox aabb1, HitBox aabb2, boolean displacement, boolean point) {
        Collision col = null;
        Rectangle r1 = aabb1.getBoundingBox();
        Rectangle r2 = aabb2.getBoundingBox();
        if (r1.overlaps(r2)) {
            Vector2 diff, pos;
            if (displacement) {
                //Vector2 po = aabb2.getCenter().getVektorTo(aabb1.getCenter());
                Vector2 po = aabb1.getCenterReuse(temp1).sub(aabb2.getCenterReuse(temp2));
                //diff = aabb1.getDimension().div(2).add(aabb2.getDimension().div(2));
                float nx = (r1.getWidth() / 2) + (r2.getWidth() / 2);
                float ny = (r1.getHeight() / 2) + (r2.getHeight() / 2);
                diff = new Vector2(nx, ny);
                if (po.x < 0) {
                    diff.x = (diff.x * -1);
                }
                if (po.y < 0) {
                    diff.y = (diff.y * -1);
                }
                diff = diff.sub(po);
                if (Math.abs(diff.x) < Math.abs(diff.y)) {
                    diff.y = 0;
                } else {
                    diff.x = 0;
                }
            } else {
                diff = null;
            }
            if (point) {
                /*Rectangle intersection = r1.intersect(r2);
                pos = intersection.getCenter();*/
                pos = null;
            } else {
                pos = null;
            }
            col = new Collision(aabb1, aabb2, diff, pos);
        }
        return col;
    }

    private static Collision calcCollisionCircleVsCircle(HitBox c1, HitBox c2, boolean displacement, boolean point) {
        Collision col = null;
        Rectangle r1 = c1.getBoundingBox();
        Rectangle r2 = c2.getBoundingBox();
        if (r1.overlaps(r2)) {
            Vector2 cc1 = c1.getCenterReuse(temp1);
            Vector2 cc2 = c2.getCenterReuse(temp2);
            double quDist = cc1.dst2(cc2);
            //Radius is now interpreted as Width/2
            double quRadi = Calc.square((r1.getWidth() + r2.getWidth()) / 2);
            if (quDist < quRadi) {
                Vector2 diff, pos;
                if (displacement || point) {
                    double dist = Math.sqrt(quDist);
                    double radi = Math.sqrt(quRadi);
                    double dir = Math.atan2(cc1.x - cc2.x, cc1.y - cc2.y);
                    if (displacement) {
                        diff = new Vector2((float) (Math.sin(dir) * (radi - dist)), (float) (Math.cos(dir) * (radi - dist)));
                    } else {
                        diff = null;
                    }
                    if (point) {
                        float length = (float) (r1.getWidth() / 2 - (radi - dist) / 2);
                        pos = new Vector2((float) (cc1.x + Math.sin(dir) * (length)), (float) (cc1.y + Math.cos(dir) * (length)));
                    } else {
                        pos = null;
                    }
                } else {
                    diff = pos = null;
                }
                col = new Collision(c1, c2, diff, pos);
            }
        }
        return col;
    }

    private static Collision calcCollisionAABBVsCircle(HitBox box, HitBox c, boolean displacement, boolean point) {
        Collision col = null;
        Rectangle r1 = box.getBoundingBox();
        Rectangle r2 = c.getBoundingBox();
        if (r1.overlaps(r2)) {
            Vector2 cc = c.getCenter();
            Vector2 cr = cc.cpy();
            clampVector(cr, r1);
            double quDist = cc.dst2(cr);
            double quRad = Calc.square(r2.width / 2);
            if (quDist < quRad) {
                Vector2 diff, pos;
                if (displacement) {
                    if (r1.contains((float) cc.x, (float) cc.y)) {
                        Vector2 blub = cc.sub(box.getCenter());
                        if (Math.abs(blub.y) < Math.abs(blub.x)) {
                            if (blub.x > 0) {
                                //cirlce near right side of AABB
                                diff = new Vector2(-r1.width / 2 + blub.x, 0);
                            } else {
                                //cirlce near left side side of AABB
                                diff = new Vector2(r1.width / 2 - blub.x, 0);
                            }
                        } else if (blub.y > 0) {
                            //cirlce near lower side side of AABB
                            diff = new Vector2(0, -r1.height / 2 + blub.y);
                        } else {
                            //cirlce near upper side side of AABB
                            diff = new Vector2(0, r1.height / 2 - blub.y);
                        }
                    } else {
                        diff = fromPolar(directionTo(cr, cc), (float) (Math.sqrt(quDist) - Math.sqrt(quRad)));
                    }
                } else {
                    diff = null;
                }
                if (point) {
                    //FIXME: make this more correct
                    pos = cr;
                } else {
                    pos = null;
                }
                col = new Collision(box, c, diff, pos);
            }
        }
        return col;
    }

    /**
     * Clamps the Vector2 to the specific area.
     * <p/>
     * Switches Min and Max if Min is larger than Max.
     * <p/>
     *
     * @param vec   The Vector to clamp.
     * @param range The Area to which constrain the Vector.
     */
    public static void clampVector(Vector2 vec, Rectangle range) {
        vec.x = (float) (Calc.fastRange(vec.x, range.x, range.x + range.width));
        vec.y = (float) (Calc.fastRange(vec.y, range.y, range.y + range.height));
    }

    /**
     * Creates a new Vector2 from Polar Coordinates.
     * <p/>
     *
     * @param direction The direction in radians
     * @param length    The length of the Vector2
     * @return A new Vector2 from Polar Coordinates
     */
    public static Vector2 fromPolar(float direction, float length) {
        return new Vector2((float) (Math.sin(direction) * length), (float) (Math.cos(direction) * length));
    }

    /**
     * Calculates the Angle to another Vector2.
     * <p/>
     *
     * @param from The Vector2 to calculate the Angle from.
     * @param to   The Vector2 to calculate the Angle to.
     * @return The Angle to that Vector2 in radians.
     */
    public static float directionTo(Vector2 from, Vector2 to) {
        float dx = to.x - from.x;
        float dy = to.y - from.y;
        return (float) Math.atan2(dx, dy);
    }
}
