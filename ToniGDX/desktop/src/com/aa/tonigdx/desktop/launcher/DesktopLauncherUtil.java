package com.aa.tonigdx.desktop.launcher;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by toni on 6/22/16.
 */
public class DesktopLauncherUtil {

    public static void runTexturePacker(){
        runTexturePacker(Texture.TextureFilter.Nearest);
    }

    public static void runTexturePacker(Texture.TextureFilter textureFilter) {
        try {
            TexturePacker.Settings settings = new TexturePacker.Settings();

            settings.filterMag = textureFilter;
            settings.filterMin = textureFilter;

            TexturePacker.process(settings, "gfx/gui/elements", "gfx/gui/", "myskin");
            TexturePacker.process(settings, "gfx/sprites", "gfx/", "texture_atlas");
        } catch (Exception ex) {
            Logger.getLogger(DesktopLauncherUtil.class.getName()).log(Level.WARNING, "An exception occurred packing textures: ", ex);
        }
    }
}
