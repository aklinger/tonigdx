package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;
import com.madgag.gif.fmsware.AnimatedGifEncoder;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GifStoreRunnable implements Runnable {

    private static final String LOG_TAG = "GIF";

    private FileHandle fileHandle;
    private Array<Pixmap> recordedFrames;
    private AnimatedGifEncoder animatedGifEncoder;

    private GifResult callback;

    public GifStoreRunnable(FileHandle fileHandle, Array<Pixmap> frames, AnimatedGifEncoder animatedGifEncoder, GifResult callback) {
        this.fileHandle = fileHandle;
        this.recordedFrames = new Array<Pixmap>(frames);
        this.callback = callback;
        this.animatedGifEncoder = animatedGifEncoder;
    }

    @Override
    public void run() {
        String filename = "";
        boolean success = false;
        try {
            Gdx.app.log(LOG_TAG, "Started exporting GIF");
            for (Pixmap p : recordedFrames) {
                flipPixmap(p);
            }
            File outputfile = storeInGif();
            filename = outputfile.getAbsolutePath();
            for (Pixmap p : recordedFrames) {
                p.dispose();
            }
            Gdx.app.log(LOG_TAG, "Finished exporting GIF");
            success = true;
        } catch (Exception ex) {
            Gdx.app.log(LOG_TAG, "Error while exporting GIF", ex);
        } finally {
            if (callback != null) {
                callback.setFilename(filename);
                callback.setSuccess(success);
                Gdx.app.postRunnable(callback);
            }
        }
    }

    public String getUniqueFilePostfix() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd-HH-mm-ss");
        return simpleDateFormat.format(now);
    }

    private File storeInGif() throws FileNotFoundException {
        File baseOutputFile = fileHandle.file();
        File parentDirectory = baseOutputFile.getParentFile();
        if (!parentDirectory.exists()) {
            boolean mkdirs = parentDirectory.mkdirs();
            if (!mkdirs) {
                Gdx.app.log(LOG_TAG, "Couldn't create directory: " + parentDirectory.getPath());
            }
        }

        BufferedImage[] bufferedImages = new BufferedImage[recordedFrames.size];
        for (int j = 0; j < recordedFrames.size; j++) {
            Pixmap p = recordedFrames.get(j);
            byte[] b = new byte[p.getPixels().remaining()];
            p.getPixels().get(b);
            changeRGBAtoBGRA(b);

            BufferedImage image = new BufferedImage(p.getWidth(), p.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
            image.setData(Raster.createRaster(image.getSampleModel(), new DataBufferByte(b, b.length), new Point()));
            bufferedImages[j] = image;
        }

        File outputFile = new File(baseOutputFile.getAbsolutePath() + "_" + getUniqueFilePostfix() + ".gif");
        animatedGifEncoder.start(outputFile.getAbsolutePath());

        for (BufferedImage bufferedImage : bufferedImages) {
            animatedGifEncoder.addFrame(bufferedImage);
        }

        animatedGifEncoder.finish();

        return outputFile;
    }

    private static void changeRGBAtoBGRA(byte[] b) {
        for (int i = 0; i * 4 < b.length; i++) {
            byte r = b[i * 4];
            byte g = b[i * 4 + 1];
            b[i * 4] = b[i * 4 + 3];
            b[i * 4 + 1] = b[i * 4 + 2];
            b[i * 4 + 2] = g;
            b[i * 4 + 3] = r;
        }
    }


    private static Pixmap flipPixmap(Pixmap pixmap) {
        int w = pixmap.getWidth();
        int h = pixmap.getHeight();
        int t;

        for (int y = 0; y < h / 2; y++) {
            for (int x = 0; x < w; x++) {
                t = pixmap.getPixel(x, y);

                pixmap.drawPixel(x, y, pixmap.getPixel(x, h - y - 1));

                pixmap.drawPixel(x, h - y - 1, t);
            }
        }
        return pixmap;
    }
}
