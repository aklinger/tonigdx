package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;

public class DesktopScreenRecorder implements ScreenRecorder {

    private boolean recording;
    private int progress;
    private Array<Pixmap> recordedFrames;

    private boolean continuousRecording;
    private boolean loopedOnce;

    public DesktopScreenRecorder() {
        recordedFrames = new Array<Pixmap>(60);
    }

    @Override
    public boolean startRecording(int maxFrames) {
        if (!recording) {
            recordedFrames.clear();
            for (int i = 0; i < maxFrames; i++) {
                Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
                recordedFrames.add(pixmap);
            }
            recording = true;
            progress = 0;
            loopedOnce = false;
            return true;
        }
        return false;
    }

    @Override
    public void finishRecording() {
        recording = false;
    }

    @Override
    public boolean recordFrame() {
        boolean didRecordFrame = false;
        if (recording) {
            if (continuousRecording && progress >= recordedFrames.size) {
                loopedOnce = true;
                progress = 0;
            }
            if (progress < recordedFrames.size) {
                recordImage();
                progress++;
                didRecordFrame = true;
            } else {
                recording = false;
            }
        }
        return didRecordFrame;
    }

    private void recordImage() {
        Pixmap pixmap = recordedFrames.get(progress);
        getFrameBufferPixmap(pixmap, 0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight());
    }

    private static Pixmap getFrameBufferPixmap(Pixmap pixmap, int x, int y, int w, int h) {
        Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);

        Gdx.gl.glReadPixels(x, y, w, h, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, pixmap.getPixels());

        return pixmap;
    }

    @Override
    public Array<Pixmap> getRecordedFrames() {
        if (continuousRecording && loopedOnce) {
            int pixCount = recordedFrames.size;
            Array<Pixmap> correctOrder = new Array<Pixmap>(pixCount);
            for (int i = 0; i < pixCount; i++) {
                correctOrder.add(recordedFrames.get((progress + i) % pixCount));
            }
            return correctOrder;
        } else if (continuousRecording) {
            Array<Pixmap> recorded = new Array<Pixmap>(progress);
            for (int i = 0; i < progress; i++) {
                recorded.add(recordedFrames.get(i));
            }
            for (int i = progress; i < recordedFrames.size; i++) {
                recordedFrames.get(i).dispose();
            }
            return recorded;
        }
        return recordedFrames;
    }

    @Override
    public boolean isRecording() {
        return recording;
    }

    public void setContinuousRecording(boolean continuousRecording) {
        this.continuousRecording = continuousRecording;
    }
}
