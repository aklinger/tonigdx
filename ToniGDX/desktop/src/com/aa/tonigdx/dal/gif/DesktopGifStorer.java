package com.aa.tonigdx.dal.gif;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.Array;
import com.madgag.gif.fmsware.AnimatedGifEncoder;

public class DesktopGifStorer implements GifEncoder {
    @Override
    public void createAnimatedGif(FileHandle file, Array<Pixmap> frames, GifResult callback) {
        AnimatedGifEncoder animatedGifEncoder = createGifEncoder();
        GifStoreRunnable gifStoreRunnable = new GifStoreRunnable(file, frames, animatedGifEncoder, callback);
        Thread thread = new Thread(gifStoreRunnable);
        thread.start();
    }

    public AnimatedGifEncoder createGifEncoder() {
        AnimatedGifEncoder animatedGifEncoder = new AnimatedGifEncoder();
        animatedGifEncoder.setFrameRate(60);
        animatedGifEncoder.setRepeat(0);
        return animatedGifEncoder;
    }
}
