**Using ToniGDX with Gradle**

To install the .jar into your local maven repository execute:

    ./gradlew install

Then you can include it in the dependencies of your parent project as:

    compile "com.aa.tonigdx:core:1.0.0"
